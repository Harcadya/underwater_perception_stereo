/*=============================================================================

  Author(s):
              Andry Maykol Gomes Pinto, 2015

  Redistribution of this file, in original or modified form, without
  prior written consent of the autor(s) is prohibited.

  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF TITLE,
  NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR  PURPOSE ARE
  DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
  AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=============================================================================*/
#include <QCoreApplication>
#include <QApplication>
#include "stereovision.h"
#include "programcontinuous.h"
#include "api_camerasetup.h"
#include <boost/shared_ptr.hpp>




int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    /* Program Of StereoVision, it contains:
     * - 2 Object Cameras
     * - 2 Object Setup
     */
    boost::shared_ptr<stereovision> vision = boost::shared_ptr<stereovision>(new stereovision());

//    ProgramContinuous cam_L(0);
//     ProgramContinuous cam_R(1);
//     //! Setup: Left Camera.
//     API_CameraSetup    * setup_L =  new API_CameraSetup(cam_L.makocamera);
//     //! Setup: Right Camera.
//     API_CameraSetup    * setup_R =  new API_CameraSetup(cam_R.makocamera);

    return app.exec();
}
