/*=============================================================================

  Author:
              Andry Maykol Gomes Pinto, 2014

  Redistribution of this file, in original or modified form, without
  prior written consent of the autor(s) is prohibited.
-------------------------------------------------------------------------------

  File:        videocapturer_alliedvision.cpp

  Description: class that represents the VimbaAPI + OpenCV for mako125C


  Files:       requires videocapturer_alliedvision.h
-------------------------------------------------------------------------------

  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF TITLE,
  NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR  PURPOSE ARE
  DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
  AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=============================================================================*/


#ifndef VIDEOCAPTURER_ALLIEDVISION_H
#define VIDEOCAPTURER_ALLIEDVISION_H
#include <iostream>
#include <queue>

#include <boost/shared_ptr.hpp>
#include <opencv2/opencv.hpp>

#include <QObject>
#include <QWidget>
#include <QMutex>
#include <QTimer>


//#include "VimbaCPP.h"
#include "makocamera.h"



#define NUM_FRAMES 1
enum
{
    CV_CAP_PROP_VMB_FPS             = 12001,
    CV_CAP_PROP_VMB_FPS_LIMIT       = 12002,
    CV_CAP_PROP_VMB_FRAME_WIDTH     = 12003,
    CV_CAP_PROP_VMB_FRAME_HEIGHT    = 12004,
    CV_CAP_PROP_VMB_PIXELFORMAT     = 12005,
    CV_CAP_PROP_VMB_GAIN_AUTO       = 12006,
    CV_CAP_PROP_VMB_GAIN_VAL        = 12007,
    CV_CAP_PROP_VMB_WB_AUTO         = 12008,
    CV_CAP_PROP_VMB_WB_VAL          = 12009,
    CV_CAP_PROP_VMB_EXPOSURE_AUTO   = 12010,
    CV_CAP_PROP_VMB_EXPOSURE_VAL    = 12011,
    CV_CAP_PROP_VMB_EXPOSURE_MIN    = 12012,
    CV_CAP_PROP_VMB_EXPOSURE_MAX    = 12013,
    CV_CAP_PROP_VMB_GAMMA           = 12014,
    CV_CAP_PROP_VMB_HUE             = 12015,
    CV_CAP_PROP_VMB_SATURATION      = 12016,
    CV_CAP_PROP_VMB_BLACKLEVEL      = 12017,
    CV_CAP_PROP_VMB_TRIGGER_SELECT  = 12018,  //selector
    CV_CAP_PROP_VMB_TRIGGER_VAL     = 12019,
    CV_CAP_PROP_VMB_TRIGGER_SOURCE  = 12020,  //source
    CV_CAP_PROP_VMB_TRIGGER_MODE    = 12021,  //mode
    CV_CAP_PROP_VMB_SUB_BOTTOM      = 12022,
    CV_CAP_PROP_VMB_SUB_LEFT        = 12023,
    CV_CAP_PROP_VMB_SUB_RIGHT       = 12024,
    CV_CAP_PROP_VMB_SUB_TOP         = 12025,
    CV_CAP_PROP_VMB_OFFSET_X        = 12026,
    CV_CAP_PROP_VMB_OFFSET_Y        = 12027,
    CV_CAP_PROP_VMB_GAIN_MIN        = 12028,
    CV_CAP_PROP_VMB_GAIN_MAX        = 12029,
    CV_CAP_PROP_VMB_ACQ_MODE        = 12030,    //acquisition mode
    CV_CAP_PROP_VMB_TEMPERATURE     = 12031

};





namespace Device {


/*! \brief Camera Properties
 */
struct CameraProperties {
    double v_Gamma, v_Saturation, v_Hue, v_blackLevel, v_gain, v_whiteBalance, v_temperature, v_exposer, v_fps, v_trigger_value;
    MakoCamera::ExposureAutoEnum v_exposermode;
    MakoCamera::GainAutoEnum v_gainmode;
    MakoCamera::BalanceWhiteAutoEnum v_wbalancemode;
    MakoCamera::AcquisitionModeEnum v_acquisitionMode;
    VmbInt64_t v_height, v_width;
    MakoCamera::PixelFormatEnum v_pixelformat;
    VmbUint32_t v_timeout_frame;                            //!< timeout during the grabbing process.
    MakoCamera::TriggerSelectorEnum v_trigger_select;
    MakoCamera::TriggerSourceEnum v_trigger_source;
    MakoCamera::TriggerModeEnum v_trigger_mode;
    cv::Point2i subRegion_br;                                    //bottom right subregion of the camera.
    cv::Point2i subRegion_tl;                                    //top left subregion of the camera.
};




/*! \brief Class that handles with changes in the ListCameras
 *  \author Andry Maykol Gomes Pinto, 2014. Based on Vimba Examples.
 */
class CameraObserver : public QObject, public AVT::VmbAPI::ICameraListObserver
{
    Q_OBJECT

public:
    CameraObserver();
    ~CameraObserver();

    virtual void CameraListChanged( AVT::VmbAPI::CameraPtr pCam, AVT::VmbAPI::UpdateTriggerType reason );

signals:
    void sig_updateDeviceList(void);

};




/*! \brief Class that handles with changes in the handling of a new frame (the call)
 *  \author Andry Maykol Gomes Pinto, 2014. Based on Vimba Examples.
 */
class FrameObserver : public QObject, virtual public AVT::VmbAPI::IFrameObserver
{
    Q_OBJECT

private:
    // Since a Qt signal cannot contain a whole frame
    // the frame observer stores all FramePtr
    std::queue <AVT::VmbAPI::FramePtr> m_Frames;
    QMutex m_FramesMutex;

public:
    FrameObserver(AVT::VmbAPI::CameraPtr pCamera );

    // This is our callback routine that will be executed on every received frame
    virtual void FrameReceived( const AVT::VmbAPI::FramePtr pFrame );

    // After the view has been notified about a new frame it can pick it up
    AVT::VmbAPI::FramePtr GetFrame();

    // Clears the double buffer frame queue
    void ClearFrameQueue();

    // "true" if there are any frame ready.
    bool isFrameReady;

signals:
    void sig_newFrame();
};







/*! \brief API between AlliedTechnologies and the OpenCV.
 *         required:
 *                  class MakoCamera -  implements the lower level procedures (interface with the camera)
 *                  Qt libraries     -  for the signal/slot : interface between the class CameraObserver and the class VideoCapturer_AlliedVision.
 *                                      Could be removed if CameraObserver is not required!
 *                  class CameraObserver  -  is a listener that detect changes in the number of cameras available at each moment.
 *                  class FrameObserver   -  is a listener that detect new frames during the async mode.
 *
 *  \warning It was implemented taking into consideration: "mako 125c", properties of other cameras may not be implemented.
 *  \author Andry Maykol Gomes Pinto, 2014.
 *
 *
 *  \note How to configure camera PoE with Ubuntu 12.04LTS
 *        1) WireConnection -> add -> Manual -> (IP) 169.254.100.243 ; (Mask) 255.255.0.0 ; (Gate) 0.0.0.0
 *        2) Open VimbaViewer -> manage the camera
 *
 */
class VideoCapturer_AlliedVision : public QObject, public cv::VideoCapture
{
    Q_OBJECT

private:
    //! DECODING: convert a frame into cv::Mat.
    VmbErrorType ConvertFrameToImage( AVT::VmbAPI::FramePtr pFrame, cv::Mat & OutImage, bool isOut12b);
    void CopyBayerToImage_12b_to_8b( VmbUchar_t *pInBuffer, cv::Mat & OutImage);      //!< Copy buffer in Bayer 12bit to 8bit cv::Mat.
    void CopyBayerToImage_12b_to_12b( VmbUchar_t *pInBuffer, cv::Mat & OutImage );    //!< Copy buffer in Bayer 12bit to 12bit cv::Mat.
    void CopyBayerToImage_8b_to_8b( VmbUchar_t *pInBuffer, cv::Mat & OutImage );      //!< Copy buffer in Bayer 8bit to 8bit cv::Mat.

    //! Internal setup.
    bool isVimbaReady;                                                                //!< true if VimbaSystem *system is ready.
    bool isCameraOpen;                                                                //!< true if camera/device is open.
    void initSetup(void);                                                             //!< initializes the setup.

    //! Info about the available devices.
    std::vector<std::string> camName, camID, camModel, camSerialN, camInterfID;
    unsigned int idx_connectedToDevice;                                               //!< idx of the device currently connected to, according to std::vector<std::string>.

    //! Device -  Camera.
    AVT::VmbAPI::shared_ptr<MakoCamera> camera;
    CameraProperties property;                                                        //!< actual configuration of the camera.

    //! CameraObserver that analyses the changes in the List of Devices.
    CameraObserver * pCameraObserver;                                                 //!< Our camera observer.
    void printListOfCameras();                                                        //!< Print List of Cameras available.


    //! FRAME retrieved from the camera.
    AVT::VmbAPI::FramePtr pFrame;

    //! Image in OpenCV. Used only for the grab.
    bool hasNewFrameToDecode;                                                         //!< Used in ".grab" and ".retrieve"


    // ASYNC
    bool isStreaming;                           //!< false if it is in singleframe.
    VmbInt64_t m_nPixelFormat;
    // Every camera has its own frame observer
    FrameObserver * pFrameObserver;
    cv::Mat img;
    bool hasNewFrameContinuous;


public:
    //! Vimba vector of cameras available.
    AVT::VmbAPI::CameraPtrVector cameras;
    //! Vimba System.
    AVT::VmbAPI::VimbaSystem *system;
    //! Vimb Error definer.
    VmbErrorType err;


    VideoCapturer_AlliedVision();
    VideoCapturer_AlliedVision(const std::string& filename);
    VideoCapturer_AlliedVision(int device);


    void get_Camera_Properties(void);                                                 //!< retrieve camera properties.
    void set_frame_Properties(void);                                                 //!< auto configuration of frame properties.


//    virtual bool open(const std::string& filename);
    virtual bool open(int device);
    virtual bool isOpened() const;
    virtual void release();

    virtual bool grab();
    virtual bool retrieve(cv::Mat& image, int channel=0);
    virtual VideoCapture& operator >> (cv::Mat& image);
    virtual bool read(cv::Mat& image);

    virtual bool set(int propId, double value);
    virtual double get(int propId);

    void close();




    bool set_WB_red(double val);
    bool set_WB_blue(double val);
    bool set_WB_mode_red(int mode);
    bool set_WB_mode_blue(int mode);
    double get_WB_red();
    double get_WB_blue();
    bool set_WB_autoOnce();
    bool set_WB_autoContinuous();

    std::string get_IDCamera();



    VmbErrorType StartContinuousImageAcquisition(int device);
    VmbErrorType StopContinuousImageAcquisition();
    bool readContinuous(cv::Mat& image);

public slots:
    void slot_updateDeviceList(void);                                                 /// SLOT: number of cameras changed, SIGNAL: CameraObserver::sig_updateDeviceList.
    void slot_FrameReady(void);                                                       /// SLOT: frame received from the FrameObserver, SIGNAL: FrameObserver::sig_FrameReceived.

signals:
    void sig_EventFrameReady(void);                        //is emitted when a new frame is ready to be printed.

};

}





#endif // VIDEOCAPTURER_ALLIEDVISION_H
