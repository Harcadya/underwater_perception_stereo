/*=============================================================================

  Author:
              Andry Maykol Gomes Pinto, 2014

  Redistribution of this file, in original or modified form, without
  prior written consent of the autor(s) is prohibited.
-------------------------------------------------------------------------------

  File:        programcontinuous.cpp

  Description: Example for the acquisition of the mako camera API + OpenCV

  Note:        The slot "slot_receivedFrame" is called when a new frame is available.

  Files:       requires videocapturer_alliedvision.h
-------------------------------------------------------------------------------

  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF TITLE,
  NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR  PURPOSE ARE
  DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
  AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=============================================================================*/


#ifndef PROGRAMCONTINUOUS_H
#define PROGRAMCONTINUOUS_H
#include <QObject>
#include <opencv2/opencv.hpp>
#include "videocapturer_alliedvision.h"
#include <QElapsedTimer>

/*! \brief  Example for Continuous acquisition of a Mako camera
*/
class ProgramContinuous : public QObject
{
    Q_OBJECT
private:
    QElapsedTimer elapsTimer;
    void initial_camera_setup(void);                        // example of the setup of the camera

public:
    ProgramContinuous(int ndevice);
    ~ProgramContinuous();
    Device::VideoCapturer_AlliedVision  * makocamera;       //camera
    cv::Mat img;                                            //output image

    float fps;  //frames per second

private slots:
    void slot_receivedFrame(void);

signals:
    void sig_frame_ready(const cv::Mat&);
};


#endif // PROGRAMCONTINUOUS_H
