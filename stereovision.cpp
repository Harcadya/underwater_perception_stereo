#include "stereovision.h"
#define ID_CAM_L 0
#define ID_CAM_R 1


stereovision::stereovision(QObject *parent) :
    QObject(parent)
{
    mode           = Stereo_None;
    n_frame        = 0;

    mako_L    =  boost::shared_ptr<ProgramContinuous>(new ProgramContinuous(ID_CAM_L));
    setup_L   =  boost::shared_ptr<API_CameraSetup> (new  API_CameraSetup(mako_L->makocamera));
    setup_L->defaultSetup();


    mako_R    =  boost::shared_ptr<ProgramContinuous>(new ProgramContinuous(ID_CAM_R));
    setup_R   =  boost::shared_ptr<API_CameraSetup>(new API_CameraSetup(mako_R->makocamera));
    setup_R->defaultSetup();


    QObject::connect(this->mako_L.get(), SIGNAL(sig_frame_ready(const cv::Mat&)), this, SLOT(slot_frame_ready_L(const cv::Mat&)));
    QObject::connect(this->mako_R.get(), SIGNAL(sig_frame_ready(const cv::Mat&)), this, SLOT(slot_frame_ready_R(const cv::Mat&)));
    QObject::connect(this, SIGNAL(sig_cicle_Do_StereoVision(void)), this, SLOT(slot_cicle_Do_StereoVision(void)));

    cv::namedWindow("Left", cv::WINDOW_NORMAL);
    cv::namedWindow("Right", cv::WINDOW_NORMAL);

    //timeout
    _timeout = new QTimer();
    _timeout->setInterval(10000);  //10 second
    QObject::connect(_timeout, SIGNAL(timeout()), this, SLOT(slot_timeout()));
}



void stereovision::slot_frame_ready_L(const cv::Mat & _img_L)
{
    //std::cout<<" L"<<std::endl;
    switch (mode)
    {
    case Stereo_None:
        img_L = _img_L.clone();  //copy each pixel
        mode  = Stereo_Only_L;
        break;

    case Stereo_Only_R:
        img_L = _img_L.clone();  //copy each pixel
        mode  = Stereo_Both;
        emit sig_cicle_Do_StereoVision();  //emit signal
        break;

    case Stereo_Only_L:
        img_L = _img_L.clone();  //copy each pixel
        mode = Stereo_Only_L;
        break;

    case Stereo_Running:
       //Do nothing-> skip the frame
       break;

    default:
        //Do nothing
        break;
    }
    return;
}



void stereovision::slot_frame_ready_R(const cv::Mat &_img_R)
{
    //std::cout<<" R"<<std::endl;
    switch (mode)
    {
    case Stereo_None:
        img_R = _img_R.clone();  //copy each pixel
        mode = Stereo_Only_R;
        break;

    case Stereo_Only_L:
        img_R = _img_R.clone();  //copy each pixel
        mode = Stereo_Both;
        emit sig_cicle_Do_StereoVision();  //emit signal
        break;

    case Stereo_Only_R:
        img_R = _img_R.clone();  //copy each pixel
        mode = Stereo_Only_R;
        break;

     case Stereo_Running:
        //Do nothing-> skip the frame
        break;

    default:
        //Do nothing
        break;
    }
    return;
}


//! TRY TO PROGRAM HERE!!!! <TODO>
void stereovision::slot_cicle_Do_StereoVision()
{
    //cv::Mat im_L = img_L, im_R = img_R;  //this not copy each pixel

    //Save the image after S (caution: must be in sync)
    char c = cv::waitKey(20);
    if( (c == 's')  || (c == 'S')  )
    {

        QString namefile_R(tr("r_%1.png").arg(n_frame));
        QString namefile_L(tr("l_%1.png").arg(n_frame));

        cv::imwrite(namefile_L.toStdString(), img_L);
        cv::imwrite(namefile_R.toStdString(), img_R);
        n_frame++;
        std::cout<<" Saved "<<namefile_L.data()<<"  "<<namefile_R.data()<<std::endl;
    }
    else
        if( (c == 'q')  || (c == 'Q')  )
        {
            exit(0);
        }


    cv::imshow("Left", img_L);
    cv::imshow("Right", img_R);

    _timeout->start();
    mode = Stereo_None;
    //std::cout<<" NewFrame"<<std::endl;

    return;
}

void stereovision::slot_timeout()
{
    mako_L->makocamera->close();
    mako_R->makocamera->close();
    mako_L->makocamera->release();
    mako_R->makocamera->release();

    mako_L.reset(new ProgramContinuous(ID_CAM_L));
    mako_R.reset(new ProgramContinuous(ID_CAM_R));

    setup_R->setCamera(mako_R->makocamera);
    setup_L->setCamera(mako_L->makocamera);
//    setup_R->defaultSetup();
//    setup_L->defaultSetup();


    QObject::connect(this->mako_L.get(), SIGNAL(sig_frame_ready(const cv::Mat&)), this, SLOT(slot_frame_ready_L(const cv::Mat&)));
    QObject::connect(this->mako_R.get(), SIGNAL(sig_frame_ready(const cv::Mat&)), this, SLOT(slot_frame_ready_R(const cv::Mat&)));
    QObject::connect(this, SIGNAL(sig_cicle_Do_StereoVision(void)), this, SLOT(slot_cicle_Do_StereoVision(void)));

    _timeout->start();
    std::cout<<" Reconnecting"<<std::endl;
    return;
}
