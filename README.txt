22-04-2015

Vimba C++ API + OpenCV

Modes: 
 1) Single Shot
 2) Continuous acquisition
 3) Setup API
 4) Prepare to add code to stereovision

Files:
 - makocamera.h 		: low level procedures 
 - videocapturer_alliedvision.h : API interface 
 - programcontinuous.h	   	: example for continuous acquisition mode
 - programsingleshot.h		: example for single acquisition mode
 - stereovision.h		: example for stereovision programs... 

email for bugs: andry.pinto@fe.up.pt
