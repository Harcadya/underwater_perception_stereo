#-------------------------------------------------
#
# Project created by QtCreator 2014-08-04T10:12:36
#
#-------------------------------------------------

QT       += core
QT       += gui
QT       += widgets

TARGET    = apimako
TEMPLATE  = app

INCLUDEPATH += /usr/local/include/ \
INCLUDEPATH += /home/andrypinto/libraries/Cameras/Vimba_1_3/ \
INCLUDEPATH += `pkg-config --cflags opencv`



#LIBS

LIBS += -L/usr/local/lib -L/home/andrypinto/libraries/Cameras/Vimba_1_3/VimbaCPP/DynamicLib/x86_64bit -lVimbaC  -lVimbaCPP\

LIBS += `pkg-config --libs opencv` \


#LIBS +=  -lopencv_core \
#         -lopencv_highgui \
#         -lopencv_imgproc \
#         -lopencv_features2d \
#         -lopencv_nonfree \
#         -lopencv_calib3d \
#         -lopencv_contrib \
#         -lopencv_flann \
#         -lopencv_gpu \
#         -lopencv_legacy \
#         -lopencv_ml \
#         -lopencv_objdetect \
#         -lopencv_ts \
#         -lopencv_video \
#         -lopencv_stitching \
#         -lboost_thread \
#         -lVimbaC \
#         -lVimbaCPP







SOURCES += main.cpp \
    makocamera.cpp \
    videocapturer_alliedvision.cpp \
    programcontinuous.cpp \
    programsingleshot.cpp \
    api_camerasetup.cpp \
    stereovision.cpp

HEADERS += \
    makocamera.h \
    videocapturer_alliedvision.h \
    programcontinuous.h \
    programsingleshot.h \
    api_camerasetup.h \
    stereovision.h
