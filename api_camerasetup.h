/*=============================================================================

  Author(s):
              Andry Maykol Gomes Pinto, 2015

  Redistribution of this file, in original or modified form, without
  prior written consent of the autor(s) is prohibited.
-------------------------------------------------------------------------------

  File:        api_camerasetup.h

  Description: Qt program that setup a camera (minimalistic approach)

  Note:

  Files:       requires videocapturer_alliedvision.h
-------------------------------------------------------------------------------

  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF TITLE,
  NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR  PURPOSE ARE
  DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
  AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=============================================================================*/


#ifndef CAMERASETUP_H
#define CAMERASETUP_H

#include <QWidget>
#include "videocapturer_alliedvision.h"
#include <QDialog>
#include <QtGui>
#include <QTimer>
#include <QByteArray>
#include <QDataStream>
#include <QLabel>
#include <QMenu>
#include <QSlider>


class QLabel;
class QPushButton;
class QUdpSocket;
class QLCDNumber;


class API_CameraSetup : public QWidget
{
    Q_OBJECT

private:
    Device::VideoCapturer_AlliedVision  * camera;       //camera
    QSlider * slider_WB_red;
    QSlider * slider_WB_blue;
    QSlider * slider_exp;
    QSlider * slider_fps;
    QSlider * slider_gain;
    QSlider * slider_res_w;
    QSlider * slider_res_h;
    QSlider * slider_offset_w;
    QSlider * slider_offset_h;
    QLabel  * label_fps_value;
    QLabel  * label_exp_value;
    QLabel  * label_gain_value;
    QLabel  * label_WB_red_value;
    QLabel  * label_WB_blue_value;
    QLabel  * label_res_w_value;
    QLabel  * label_res_h_value;
    QLabel  * label_offset_w_value;
    QLabel  * label_offset_h_value;

public:
    explicit API_CameraSetup(Device::VideoCapturer_AlliedVision * _camera, QWidget *parent = 0);
    QWidget *windowVision;

    //! atribute a camera to the setup.
    void setCamera(Device::VideoCapturer_AlliedVision * _camera);

    //! Default Setting: Modify initial parameters for the camera.
    void defaultSetup();  //default setup of the camera.


signals:

private slots:
    void slot_slider_exp_changed(int val);
    void slot_slider_fps_changed(int val);
    void slot_slider_gain_changed(int val);
    void slot_slider_WB_blue_changed(int val);
    void slot_slider_WB_red_changed(int val);
    void slot_slider_res_w_changed(int val);
    void slot_slider_res_h_changed(int val);
    void slot_slider_offset_w_changed(int val);
    void slot_slider_offset_h_changed(int val);

};

#endif // CAMERASETUP_H
