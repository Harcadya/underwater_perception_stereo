/*=============================================================================

  Author:
              Andry Maykol Gomes Pinto, 2015

  Redistribution of this file, in original or modified form, without
  prior written consent of the autor(s) is prohibited.
-------------------------------------------------------------------------------

  File:        stereovision.h

  Description: Qt program that captures a pair of images using a mako API camera and computes the sterescopy.

  Note:        OpenCV and Qt

  Files:       requires videocapturer_alliedvision.h, programcontinuous.h programsingleshot.h api_camerasetup.h
-------------------------------------------------------------------------------

  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF TITLE,
  NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR  PURPOSE ARE
  DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
  AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=============================================================================*/

#ifndef STEREOVISION_H
#define STEREOVISION_H

#include <QObject>
#include <QMutex>
#include <QTimer>

#include <opencv2/opencv.hpp>


#include "programcontinuous.h"
#include "programsingleshot.h"
#include "api_camerasetup.h"
#include <boost/shared_ptr.hpp>

enum _StereoProgramMode { Stereo_Only_L,     //only left image was received
                          Stereo_Only_R,     //only right image was received
                          Stereo_Both,       //both images were received
                          Stereo_None,       //none image was received
                          Stereo_Running     // when the stere is processing
                        };


/** @brief Class that computes the stereoscopic with two online cameras.
 *
 *  @note press 's' or 'S' to save the pair of images (left and right).
 *  @note slot_cicle_Do_StereoVision is active when the pair of images is received.
 */
class stereovision : public QObject
{
    Q_OBJECT

private:
    _StereoProgramMode mode;

    //! Device: Left Camera.
    boost::shared_ptr<ProgramContinuous>  mako_L;
    //! Device: Right Camera.
    boost::shared_ptr<ProgramContinuous>  mako_R;
    //! Setup: Left Camera.
    boost::shared_ptr<API_CameraSetup>    setup_L;
    //! Setup: Right Camera.
    boost::shared_ptr<API_CameraSetup>    setup_R;

    int n_frame;  // auxiliar variable for saving the pair of images. 

    //! timeout mechanism
    QTimer * _timeout;

public:
    //! Constructor.
    explicit stereovision(QObject *parent = 0);

    cv::Mat img_L;
    cv::Mat img_R;

signals:
    void sig_cicle_Do_StereoVision();  //! signal for activate the cicle for computing the stereovision.

private slots:

    /** @brief Main cicle -  executed when both images are received.

    @param cv::Mat &_left Input Image from the left camera.
    @param cv::Mat &_right Input Image from the right camera.
     */
    void slot_cicle_Do_StereoVision(); //! slot for activate the cicle for computing the stereovision.

    /** @brief Timeout for reconnecting cameras is needed.
     */
    void slot_timeout();               //! slot for reconnecting cameras.

public slots:
    void slot_frame_ready_L(const cv::Mat& _img_L); //! sig  - active when it receives frame from Left Camera.
    void slot_frame_ready_R(const cv::Mat& _img_R); //! sig  - active when it receives frame from Right Camera.
};

#endif // STEREOVISION_H
