#include "programsingleshot.h"

#define TIME_CICLE 20



ProgramSingleShot::ProgramSingleShot(int ndevice)
{
    makocamera =  new Device::VideoCapturer_AlliedVision(ndevice);

    cv::Mat img_null(1,1, CV_8UC3, cv::Scalar::all(255));
    cv::imshow("Live", img_null);

    //initial configuration of the camera.
    initial_camera_setup();

    timer_cicle = new QTimer();
    //connect the signal/slots: "new frame"
    QObject::connect( timer_cicle, SIGNAL( timeout(void) ), this, SLOT( slot_cicle(void)) );
    timer_cicle->start(TIME_CICLE);
}

ProgramSingleShot::~ProgramSingleShot()
{
    //! Close.
    makocamera->release();
}

void ProgramSingleShot::slot_cicle()
{
    if(makocamera->isOpened()) //Initialization of the API + Open Camera
    {
        //! Get Frame.
        bool hasReceived = makocamera->read(img);


        t_time =  ((double) cv::getTickCount() - t_time) / ((double) cv::getTickFrequency());
        std::cout<<" FPS="<<1.0 / t_time<<std::endl;


        int key = cv::waitKey(10);
        if(key == 'q')
            timer_cicle->stop();
        else
            timer_cicle->start();

        if(hasReceived)
            cv::imshow("Live", img);

        t_time =  (double) cv::getTickCount();
    }
    return;
}

void ProgramSingleShot::initial_camera_setup()
{
    if(makocamera->isOpened())
    {
        //! Set Basic Setup.
        makocamera->set_frame_Properties();


        if(!makocamera->set(CV_CAP_PROP_VMB_FRAME_WIDTH, 1066))
            std::cout<<" CV_CAP_PROP_VMB_FRAME_WIDTH fail"<<std::endl;
        if(!makocamera->set(CV_CAP_PROP_VMB_FRAME_HEIGHT, 768))
            std::cout<<" CV_CAP_PROP_VMB_GAIN_VAL fail"<<std::endl;

        if(!makocamera->set(CV_CAP_PROP_VMB_TRIGGER_SOURCE, (double) MakoCamera::TriggerSource_FixedRate))
            std::cout<<" TRIGGER_SOURCE fail"<<std::endl;

        //! Read Basic Setup.
        makocamera->get_Camera_Properties();



        /*
        if(!makocamera->set(CV_CAP_PROP_VMB_GAIN_AUTO, MakoCamera::GainAuto_Once))
            std::cout<<" CV_CAP_PROP_VMB_GAIN_AUTO fail"<<std::endl;
        if(!makocamera->set(CV_CAP_PROP_VMB_GAIN_VAL, 0))
            std::cout<<" CV_CAP_PROP_VMB_GAIN_VAL fail"<<std::endl;

        if(!makocamera->set(CV_CAP_PROP_VMB_EXPOSURE_AUTO, MakoCamera::ExposureAuto_Once))
            std::cout<<" EXPOSUREAUTO_SET fail"<<std::endl;
        if(!makocamera->set(CV_CAP_PROP_VMB_EXPOSURE_MAX, 19200.0 ))  //for auto
            std::cout<<" EXPOSUREMAX_SET fail"<<std::endl;
        if(!makocamera->set(CV_CAP_PROP_VMB_EXPOSURE_MIN, 10000.0 ))  //for auto
            std::cout<<" EXPOSUREMIN_SET fail"<<std::endl;
        if(!makocamera->set(CV_CAP_PROP_VMB_EXPOSURE_VAL, 18000.0 ))
            std::cout<<" EXPOSUREAUTO_SET fail"<<std::endl;

        if(!makocamera->set(CV_CAP_PROP_VMB_WB_AUTO, MakoCamera::BalanceWhiteAuto_Once ))  //for auto
            std::cout<<" EXPOSUREMIN_SET fail"<<std::endl;
        if(!makocamera->set(CV_CAP_PROP_VMB_WB_VAL, 1.60 ))  //for auto
            std::cout<<" EXPOSUREMIN_SET fail"<<std::endl;
        */
    }
    return;
}
