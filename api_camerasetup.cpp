#include "api_camerasetup.h"

API_CameraSetup::API_CameraSetup(Device::VideoCapturer_AlliedVision *_camera, QWidget *parent) :
    QWidget(parent)
{   
    try
    {
        /// LAYOUT
        QGridLayout *mainLayout = new QGridLayout;

        QLabel * label_img      = new QLabel();
        label_img->setText("Cameras");
        mainLayout->addWidget(label_img, 0, 0, 1, 4);



        //FPS
        QLabel * label_fps      = new QLabel();
        label_fps->setText("FPS");
        mainLayout->addWidget(label_fps, 1, 0, 1, 1);

        slider_fps = new QSlider(Qt::Horizontal);
        slider_fps->setMaximum(300);
        slider_fps->setMinimum(10);
        slider_fps->setTickPosition(QSlider::TicksBothSides);
        mainLayout->addWidget(slider_fps, 1, 1, 1, 3);

        label_fps_value      = new QLabel();
        label_fps_value->setText(tr("%1").arg((float) slider_fps->value()/10));
        mainLayout->addWidget(label_fps_value, 1, 4, 1, 1);





        //Expouser
        QLabel * label_exp      = new QLabel();
        label_exp->setText("Expouser");
        mainLayout->addWidget(label_exp, 2, 0, 1, 1);

        slider_exp =  new QSlider(Qt::Horizontal);
        slider_exp->setMaximum(1500000);
        slider_exp->setMinimum(3000);
        slider_exp->setTickPosition(QSlider::TicksBothSides);
        mainLayout->addWidget(slider_exp, 2, 1, 1, 3);

        label_exp_value      = new QLabel();
        label_exp_value->setText(tr("%1").arg((float) slider_fps->value()/10));
        mainLayout->addWidget(label_exp_value, 2, 4, 1, 1);



        //Gain
        QLabel * label_gain      = new QLabel();
        label_gain->setText("Gain");
        mainLayout->addWidget(label_gain, 3, 0, 1, 1);

        slider_gain =  new QSlider(Qt::Horizontal);
        slider_gain->setMaximum(10);
        slider_gain->setMinimum(0);
        slider_gain->setTickPosition(QSlider::TicksBothSides);
        mainLayout->addWidget(slider_gain, 3, 1, 1, 3);

        label_gain_value      = new QLabel();
        label_gain_value->setText(tr("%1").arg(slider_fps->value()));
        mainLayout->addWidget(label_gain_value, 3, 4, 1, 1);




        //WB Blue
        QLabel * label_WB_blue      = new QLabel();
        label_WB_blue->setText("WB Blue");
        mainLayout->addWidget(label_WB_blue, 4, 0, 1, 1);

        slider_WB_blue =  new QSlider(Qt::Horizontal);
        slider_WB_blue->setMaximum(300);
        slider_WB_blue->setMinimum(10);
        slider_WB_blue->setTickPosition(QSlider::TicksBothSides);
        mainLayout->addWidget(slider_WB_blue, 4, 1, 1, 3);

        label_WB_blue_value      = new QLabel();
        label_WB_blue_value->setText(tr("%1").arg((float)slider_fps->value()/100));
        mainLayout->addWidget(label_WB_blue_value, 4, 4, 1, 1);

        //WB Red
        QLabel * label_WB_red      = new QLabel();
        label_WB_red->setText("WB Red");
        mainLayout->addWidget(label_WB_red, 5, 0, 1, 1);

        slider_WB_red =  new QSlider(Qt::Horizontal);
        slider_WB_red->setMaximum(300);
        slider_WB_red->setMinimum(10);
        slider_WB_red->setTickPosition(QSlider::TicksBothSides);
        mainLayout->addWidget(slider_WB_red, 5, 1, 1, 3);

        label_WB_red_value      = new QLabel();
        label_WB_red_value->setText(tr("%1").arg((float)slider_fps->value()/100));
        mainLayout->addWidget(label_WB_red_value, 5, 4, 1, 1);


        //  RESOLUTION
        QLabel * label_res_w      = new QLabel();
        label_res_w->setText("Res W:");
        mainLayout->addWidget(label_res_w, 6, 0, 1, 1);

        slider_res_w =  new QSlider(Qt::Horizontal);
        slider_res_w->setMaximum(1292);
        slider_res_w->setMinimum(1);
        slider_res_w->setTickPosition(QSlider::TicksBelow);
        mainLayout->addWidget(slider_res_w, 6, 1, 1, 3);

        label_res_w_value      = new QLabel();
        label_res_w_value->setText(tr("%1").arg(slider_res_w->value()));
        mainLayout->addWidget(label_res_w_value, 6, 4, 1, 1);



        QLabel * label_res_h      = new QLabel();
        label_res_h->setText("Res H:");
        mainLayout->addWidget(label_res_w, 7, 0, 1, 1);

        slider_res_h =  new QSlider(Qt::Horizontal);
        slider_res_h->setMaximum(964);
        slider_res_h->setMinimum(1);
        slider_res_h->setTickPosition(QSlider::TicksBelow);
        mainLayout->addWidget(slider_res_h, 7, 1, 1, 3);

        label_res_h_value      = new QLabel();
        label_res_h_value->setText(tr("%1").arg(slider_res_h->value()));
        mainLayout->addWidget(label_res_h_value, 7, 4, 1, 1);






        //  OFFSET
        QLabel * label_offset_w      = new QLabel();
        label_offset_w->setText("Offset W:");
        mainLayout->addWidget(label_offset_w, 8, 0, 1, 1);

        slider_offset_w =  new QSlider(Qt::Horizontal);
        slider_offset_w->setMaximum(800);
        slider_offset_w->setMinimum(0);
        slider_offset_w->setTickPosition(QSlider::TicksBelow);
        mainLayout->addWidget(slider_offset_w, 8, 1, 1, 3);

        label_offset_w_value      = new QLabel();
        label_offset_w_value->setText(tr("%1").arg(slider_offset_w->value()));
        mainLayout->addWidget(label_offset_w_value, 8, 4, 1, 1);



        QLabel * label_offset_h      = new QLabel();
        label_offset_h->setText("Offset H:");
        mainLayout->addWidget(label_offset_h, 9, 0, 1, 1);

        slider_offset_h =  new QSlider(Qt::Horizontal);
        slider_offset_h->setMaximum(500);
        slider_offset_h->setMinimum(0);
        slider_offset_h->setTickPosition(QSlider::TicksBelow);
        mainLayout->addWidget(slider_offset_h, 9, 1, 1, 3);

        label_offset_h_value      = new QLabel();
        label_offset_h_value->setText(tr("%1").arg(slider_offset_h->value()));
        mainLayout->addWidget(label_offset_h_value, 9, 4, 1, 1);


        /// SET LAYOUT
        setLayout(mainLayout);
        setWindowFlags(Qt::WindowTitleHint | Qt::CustomizeWindowHint); //close the min,max, close buttons


        camera = _camera;
        if(camera->isOpened())
        {
            setWindowTitle(tr("Camera Setup [Main] %1").arg(camera->get_IDCamera().data()));
            show();
        }

        QObject::connect(this->slider_exp, SIGNAL(valueChanged(int)), this, SLOT(slot_slider_exp_changed(int)));
        QObject::connect(this->slider_fps, SIGNAL(valueChanged(int)), this, SLOT(slot_slider_fps_changed(int)));
        QObject::connect(this->slider_gain, SIGNAL(valueChanged(int)), this, SLOT(slot_slider_gain_changed(int)));
        QObject::connect(this->slider_WB_blue, SIGNAL(valueChanged(int)), this, SLOT(slot_slider_WB_blue_changed(int)));
        QObject::connect(this->slider_WB_red, SIGNAL(valueChanged(int)), this, SLOT(slot_slider_WB_red_changed(int)));

        QObject::connect(this->slider_res_w, SIGNAL(valueChanged(int)), this, SLOT(slot_slider_res_w_changed(int)));
        QObject::connect(this->slider_res_h, SIGNAL(valueChanged(int)), this, SLOT(slot_slider_res_h_changed(int)));
        QObject::connect(this->slider_offset_w, SIGNAL(valueChanged(int)), this, SLOT(slot_slider_offset_w_changed(int)));
        QObject::connect(this->slider_offset_h, SIGNAL(valueChanged(int)), this, SLOT(slot_slider_offset_h_changed(int)));
    }
    catch (const char* msg) {
        std::cerr << msg << std::endl;
    }


}

void API_CameraSetup::setCamera(Device::VideoCapturer_AlliedVision *_camera)
{
    camera = _camera;
    return;
}

void API_CameraSetup::defaultSetup()
{
//    camera->set(CV_CAP_PROP_VMB_GAIN_AUTO, 3);      // GAIN Auto Once;
//    camera->set(CV_CAP_PROP_VMB_EXPOSURE_AUTO, 3);  // Expouser Auto Once;
//    camera->set(CV_CAP_PROP_VMB_WB_AUTO , 3);       // ONCE


    slot_slider_exp_changed(351358);
    slot_slider_fps_changed(280);
    slot_slider_gain_changed(7);
    slot_slider_WB_blue_changed(267);
    slot_slider_WB_red_changed(300);
    slot_slider_res_h_changed(964);
    slot_slider_res_w_changed(1292);
    slot_slider_offset_h_changed(0);
    slot_slider_offset_w_changed(0);

    return;
}



void API_CameraSetup::slot_slider_exp_changed(int val)
{
    slider_exp->setValue(val);
    camera->set(CV_CAP_PROP_VMB_EXPOSURE_VAL, (double) slider_exp->value()/10.0);
    label_exp_value->setText(tr("%1").arg((float) slider_exp->value()/10));
    return;
}

void API_CameraSetup::slot_slider_fps_changed(int val)
{
    slider_fps->setValue(val);
    camera->set(CV_CAP_PROP_VMB_FPS, (double) slider_fps->value()/10);
    label_fps_value->setText(tr("%1").arg((float) slider_fps->value()/10));
    return;
}


void API_CameraSetup::slot_slider_gain_changed(int val)
{
    slider_gain->setValue(val);
    camera->set(CV_CAP_PROP_VMB_GAIN_VAL, (double) slider_gain->value());
    label_gain_value->setText(tr("%1").arg((float) slider_gain->value()));
    return;
}

void API_CameraSetup::slot_slider_WB_blue_changed(int val)
{
    slider_WB_blue->setValue(val);
    camera->set_WB_blue((double) slider_WB_blue->value()/100);
    label_WB_blue_value->setText(tr("%1").arg((float) slider_WB_blue->value()/100));
    return;
}

void API_CameraSetup::slot_slider_WB_red_changed(int val)
{
    slider_WB_red->setValue(val);
    camera->set_WB_red((double) slider_WB_red->value()/100);
    label_WB_red_value->setText(tr("%1").arg((float) slider_WB_red->value()/100));
    return;
}

void API_CameraSetup::slot_slider_res_w_changed(int val)
{
    slider_res_w->setValue(val);
    camera->set(CV_CAP_PROP_VMB_FRAME_WIDTH, (double) slider_res_w->value());
    label_res_w_value->setText(tr("%1").arg((slider_res_w->value())));
    return;
}

void API_CameraSetup::slot_slider_res_h_changed(int val)
{
    slider_res_h->setValue(val);
    camera->set(CV_CAP_PROP_VMB_FRAME_HEIGHT, (double) slider_res_h->value());
    label_res_h_value->setText(tr("%1").arg((slider_res_h->value())));
    return;
}

void API_CameraSetup::slot_slider_offset_w_changed(int val)
{
    slider_offset_w->setValue(val);
    camera->set(CV_CAP_PROP_VMB_OFFSET_X, (double) slider_offset_w->value());
    label_offset_w_value->setText(tr("%1").arg((slider_offset_w->value())));
    return;
}

void API_CameraSetup::slot_slider_offset_h_changed(int val)
{
    slider_offset_h->setValue(val);
    camera->set(CV_CAP_PROP_VMB_OFFSET_Y, (double) slider_offset_h->value());
    label_offset_h_value->setText(tr("%1").arg((slider_offset_h->value())));
    return;
}
