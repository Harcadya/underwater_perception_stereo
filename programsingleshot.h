/*=============================================================================

  Author:
              Andry Maykol Gomes Pinto, 2014

  Redistribution of this file, in original or modified form, without
  prior written consent of the autor(s) is prohibited.
-------------------------------------------------------------------------------

  File:        programsingleshot.cpp

  Description: Example for the acquisition of the mako camera API + OpenCV

  Note:        The slot "slot_cicle" is called when a timer_cicle is timeout.

  Files:       requires videocapturer_alliedvision.h
-------------------------------------------------------------------------------

  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF TITLE,
  NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR  PURPOSE ARE
  DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
  AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=============================================================================*/

#ifndef PROGRAMSINGLESHOT_H
#define PROGRAMSINGLESHOT_H
#include <QObject>
#include <QTimer>
#include <opencv2/opencv.hpp>
#include "videocapturer_alliedvision.h"

//class QTimer;

/*! \brief  Example for Single Shot acquisition of a Mako camera
*/
class ProgramSingleShot : public QObject
{
    Q_OBJECT
private:
    double t_time;                                        // measure the FPS
    void initial_camera_setup(void);                      // example of the setup of the camera
    QTimer * timer_cicle;                                 // manual acquisition of the frame -> slot_cicle

public:
    ProgramSingleShot(int ndevice);
    ~ProgramSingleShot();
    Device::VideoCapturer_AlliedVision  * makocamera;     //camera
    cv::Mat img;                                          //output image

private slots:
    void slot_cicle(void);                                //SLOT that is called for timer_cicle.timeout()
};

#endif // PROGRAMSINGLESHOT_H
