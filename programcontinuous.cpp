#include "programcontinuous.h"


ProgramContinuous::ProgramContinuous(int ndevice )
{
    makocamera =  new Device::VideoCapturer_AlliedVision(ndevice);

    //connect the signal/slots: "new frame"
    QObject::connect( this->makocamera, SIGNAL( sig_EventFrameReady(void)), this, SLOT( slot_receivedFrame(void)) );
    //initial configuration of the camera.
    initial_camera_setup();

    makocamera->StartContinuousImageAcquisition(ndevice);



    cv::Mat img_null(1,1, CV_8UC3, cv::Scalar::all(255));
    //cv::imshow("Live Cam:" + makocamera->get_IDCamera(), img_null);
}

ProgramContinuous::~ProgramContinuous()
{
    makocamera->release();     //! Close.
}



void ProgramContinuous::slot_receivedFrame(void)
{
    double t_time_fps = (double) elapsTimer.elapsed() / 1000.0;
    //std::cout<<"FPS: "<<makocamera->get_IDCamera()<<"  ="<<1.0 / t_time_fps<<std::endl;
    fps = 1.0 / t_time_fps;


    if(makocamera->isOpened())
    {
        //! Get Frame.
        bool hasReceived = makocamera->readContinuous(img);
        emit sig_frame_ready(img);  //emit signal to upper objects.

        if(hasReceived)
        {
            //std::string namewindow = "Live" + makocamera->camSerialN[0];
            //cv::imshow("Live Cam:" + makocamera->get_IDCamera(), img);
        }
    }
    elapsTimer.restart();

    return;
}

void ProgramContinuous::initial_camera_setup()
{
    if(makocamera->isOpened())
    {
        //! Set Basic Setup.
        makocamera->set_frame_Properties();

        if(!makocamera->set(CV_CAP_PROP_VMB_FRAME_WIDTH, 1292))
            std::cout<<" CV_CAP_PROP_VMB_FRAME_WIDTH fail"<<std::endl;
        if(!makocamera->set(CV_CAP_PROP_VMB_FRAME_HEIGHT, 964))
            std::cout<<" CV_CAP_PROP_VMB_GAIN_VAL fail"<<std::endl;

        if(!makocamera->set(CV_CAP_PROP_VMB_FPS, 28.0))
            std::cout<<" FPS_SET fail"<<std::endl;

        if(!makocamera->set(CV_CAP_PROP_VMB_FPS_LIMIT, 28.0))
            std::cout<<" CV_CAP_PROP_VMB_FPS_LIMIT fail"<<std::endl;


        if(!makocamera->set(CV_CAP_PROP_VMB_TRIGGER_MODE, (double) MakoCamera::TriggerMode_Off))
            std::cout<<" TRIGGER_MODE fail"<<std::endl;

        if(!makocamera->set(CV_CAP_PROP_VMB_TRIGGER_SOURCE, (double) MakoCamera::TriggerSource_FixedRate))
            std::cout<<" TRIGGER_SOURCE fail"<<std::endl;

        if(!makocamera->set(CV_CAP_PROP_VMB_TRIGGER_SELECT, (double) MakoCamera::TriggerSelector_FrameStart))
            std::cout<<" TRIGGER_SELECT fail"<<std::endl;


        //! Read Basic Setup.
        makocamera->get_Camera_Properties();

        /*
        if(!makocamera->set(CV_CAP_PROP_VMB_GAIN_AUTO, MakoCamera::GainAuto_Once))
            std::cout<<" CV_CAP_PROP_VMB_GAIN_AUTO fail"<<std::endl;
        if(!makocamera->set(CV_CAP_PROP_VMB_GAIN_VAL, 0))
            std::cout<<" CV_CAP_PROP_VMB_GAIN_VAL fail"<<std::endl;

        if(!makocamera->set(CV_CAP_PROP_VMB_EXPOSURE_AUTO, MakoCamera::ExposureAuto_Once))
            std::cout<<" EXPOSUREAUTO_SET fail"<<std::endl;
        if(!makocamera->set(CV_CAP_PROP_VMB_EXPOSURE_MAX, 19200.0 ))  //for auto
            std::cout<<" EXPOSUREMAX_SET fail"<<std::endl;
        if(!makocamera->set(CV_CAP_PROP_VMB_EXPOSURE_MIN, 10000.0 ))  //for auto
            std::cout<<" EXPOSUREMIN_SET fail"<<std::endl;
        if(!makocamera->set(CV_CAP_PROP_VMB_EXPOSURE_VAL, 18000.0 ))
            std::cout<<" EXPOSUREAUTO_SET fail"<<std::endl;

        if(!makocamera->set(CV_CAP_PROP_VMB_WB_AUTO, MakoCamera::BalanceWhiteAuto_Once ))  //for auto
            std::cout<<" EXPOSUREMIN_SET fail"<<std::endl;
        if(!makocamera->set(CV_CAP_PROP_VMB_WB_VAL, 1.60 ))  //for auto
            std::cout<<" EXPOSUREMIN_SET fail"<<std::endl;
        */
    }
    return;
}
