#include "videocapturer_alliedvision.h"
#include "makocamera.h"

namespace Device
{


VideoCapturer_AlliedVision::VideoCapturer_AlliedVision()
{
    isVimbaReady             = false;
    isCameraOpen             = false;
    isStreaming              = false;
    hasNewFrameToDecode      = false;
    hasNewFrameContinuous    = false;
    property.v_timeout_frame = 5;                               //a smaller value corrupts the image!!!!!!60

    pCameraObserver          = new CameraObserver();
    // Connect new camera found event with event handler
    QObject::connect( this->pCameraObserver, SIGNAL( sig_updateDeviceList(void)), this, SLOT( slot_updateDeviceList(void)) );

    initSetup();
}

VideoCapturer_AlliedVision::VideoCapturer_AlliedVision(const std::string &filename)
{
}

VideoCapturer_AlliedVision::VideoCapturer_AlliedVision(int device)
{
    isVimbaReady             = false;
    isCameraOpen             = false;
    isStreaming              = false;
    hasNewFrameToDecode      = false;
    hasNewFrameContinuous    = false;


    property.v_timeout_frame = 5;                               //a smaller value corrupts the image!!!!!!60

    pCameraObserver = new CameraObserver();

    // Connect new camera found event with event handler
    QObject::connect( this->pCameraObserver, SIGNAL( sig_updateDeviceList(void)), this, SLOT(slot_updateDeviceList(void)) );

    initSetup();             // start Vimba system

    this->open(device);

    // Creates a frame observer for this camera (This will be wrapped in a shared_ptr so we don't delete it)
    pFrameObserver     = new FrameObserver(camera);
}

bool VideoCapturer_AlliedVision::open(int device)
{
    isCameraOpen = false;
    if (!isVimbaReady)
        initSetup();         // start Vimba system

    if (device > camID.size())
    {
        return isCameraOpen;
        std::cerr<<" VideoCapturer_AlliedVision :: open :: device not found"<<std::endl;
    }
    else
    {
        idx_connectedToDevice = device;

        //! Create Camera.
        if(camera == NULL)
            camera = AVT::VmbAPI::shared_ptr<MakoCamera>(new MakoCamera(camID[idx_connectedToDevice].c_str(), camName[idx_connectedToDevice].c_str(), camModel[idx_connectedToDevice].c_str(), camSerialN[idx_connectedToDevice].c_str(), camInterfID[idx_connectedToDevice].c_str(), VmbInterfaceEthernet, NULL, NULL, VmbAccessModeFull));
        else
            camera.reset(new MakoCamera(camID[idx_connectedToDevice].c_str(), camName[idx_connectedToDevice].c_str(), camModel[idx_connectedToDevice].c_str(), camSerialN[idx_connectedToDevice].c_str(), camInterfID[idx_connectedToDevice].c_str(), VmbInterfaceEthernet, NULL, NULL, VmbAccessModeFull));

        //! Open.
        err = camera->Open(VmbAccessModeFull);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: could not open the camera:"<<device<<std::endl;
        else
        {
            idx_connectedToDevice = device;
            isCameraOpen = true;
        }
    }
    return isCameraOpen;
}

bool VideoCapturer_AlliedVision::isOpened() const
{
    return isCameraOpen;
}

void VideoCapturer_AlliedVision::release()
{
    if(isCameraOpen)
        camera->Close();
    if(isVimbaReady)
        system->Shutdown();

    camID.clear();
    camName.clear();
    camModel.clear();
    camSerialN.clear();
    camInterfID.clear();
    cameras.clear();

}



void VideoCapturer_AlliedVision::initSetup()
{
    isVimbaReady    = false;
    isCameraOpen    = false;

    system          =  &AVT::VmbAPI::VimbaSystem::GetInstance(); //Reference to the Vimba System

    err = system->Startup();                                     //Initialization of the API
    if(err != VmbErrorSuccess)
        std::cerr<<" VideoCapturer_AlliedVision :: Could not start VimbaSystem"<<std::endl;
    else
    {
        isVimbaReady = true;
        printListOfCameras();
    }
    return;
}



void VideoCapturer_AlliedVision::get_Camera_Properties()
{
    //Shoud be done at start
    //Frame Info
    err = camera->GetPixelFormat(property.v_pixelformat);
    if(VmbErrorSuccess != err)
        std::cerr<<"MakoCamera :: error on GetPixelFormat"<<std::endl;
    err = camera->GetWidth(property.v_width);
    if(VmbErrorSuccess != err)
        std::cerr<<"MakoCamera :: error on GetWidth"<<std::endl;
    err = camera->GetHeight(property.v_height);
    if(VmbErrorSuccess != err)
        std::cerr<<"MakoCamera :: error on GetHeight"<<std::endl;



    // Print Info.
    std::cout<<"\n\n Reading basic configuration...\n"<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_FPS:"<<this->get(CV_CAP_PROP_VMB_FPS)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_FPS_LIMIT:"<<this->get(CV_CAP_PROP_VMB_FPS_LIMIT)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_FRAME_WIDTH:"<<this->get(CV_CAP_PROP_VMB_FRAME_WIDTH)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_FRAME_HEIGHT:"<<this->get(CV_CAP_PROP_VMB_FRAME_HEIGHT)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_PIXELFORMAT:"<<this->get(CV_CAP_PROP_VMB_PIXELFORMAT)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_GAIN_AUTO:"<<this->get(CV_CAP_PROP_VMB_GAIN_AUTO)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_GAIN_VAL:"<<this->get(CV_CAP_PROP_VMB_GAIN_VAL)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_WB_AUTO:"<<this->get(CV_CAP_PROP_VMB_WB_AUTO)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_WB_VAL:"<<this->get(CV_CAP_PROP_VMB_WB_VAL)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_EXPOSURE_AUTO:"<<this->get(CV_CAP_PROP_VMB_EXPOSURE_AUTO)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_EXPOSURE_VAL:"<<this->get(CV_CAP_PROP_VMB_EXPOSURE_VAL)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_EXPOSURE_MIN:"<<this->get(CV_CAP_PROP_VMB_EXPOSURE_MIN)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_EXPOSURE_MAX:"<<this->get(CV_CAP_PROP_VMB_EXPOSURE_MAX)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_GAMMA:"<<this->get(CV_CAP_PROP_VMB_GAMMA)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_HUE:"<<this->get(CV_CAP_PROP_VMB_HUE)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_SATURATION:"<<this->get(CV_CAP_PROP_VMB_SATURATION)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_BLACKLEVEL:"<<this->get(CV_CAP_PROP_VMB_BLACKLEVEL)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_TRIGGER_SELECT:"<<this->get(CV_CAP_PROP_VMB_TRIGGER_SELECT)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_TRIGGER_MODE:"<<this->get(CV_CAP_PROP_VMB_TRIGGER_MODE)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_TRIGGER_SOURCE:"<<this->get(CV_CAP_PROP_VMB_TRIGGER_SOURCE)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_SUB_BOTTOM:"<<this->get(CV_CAP_PROP_VMB_SUB_BOTTOM)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_SUB_LEFT:"<<this->get(CV_CAP_PROP_VMB_SUB_LEFT)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_SUB_RIGHT:"<<this->get(CV_CAP_PROP_VMB_SUB_RIGHT)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_SUB_TOP:"<<this->get(CV_CAP_PROP_VMB_SUB_TOP)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_OFFSET_X:"<<this->get(CV_CAP_PROP_VMB_OFFSET_X)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_OFFSET_Y:"<<this->get(CV_CAP_PROP_VMB_OFFSET_Y)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_GAIN_MIN:"<<this->get(CV_CAP_PROP_VMB_GAIN_MIN)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_GAIN_MAX:"<<this->get(CV_CAP_PROP_VMB_GAIN_MAX)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_ACQ_MODE:"<<this->get(CV_CAP_PROP_VMB_ACQ_MODE)<<std::endl;
    std::cout<<" CV_CAP_PROP_VMB_TEMPERATURE:"<<this->get(CV_CAP_PROP_VMB_TEMPERATURE)<<std::endl;
    return;
}


/*!
  \brief Get parameters of the camera.

  \param propId is the indicator of the parameter to read.
  \return -1 if it was not possible to read a faithfull value.

  \note Possible values of the "propId":
    CV_CAP_PROP_VMB_FPS
    CV_CAP_PROP_VMB_FPS_LIMIT
    CV_CAP_PROP_VMB_FRAME_WIDTH
    CV_CAP_PROP_VMB_FRAME_HEIGHT
    CV_CAP_PROP_VMB_PIXELFORMAT
    CV_CAP_PROP_VMB_GAIN_AUTO
    CV_CAP_PROP_VMB_GAIN_VAL
    CV_CAP_PROP_VMB_WB_AUTO
    CV_CAP_PROP_VMB_WB_VAL
    CV_CAP_PROP_VMB_EXPOSURE_AUTO
    CV_CAP_PROP_VMB_EXPOSURE_VAL
    CV_CAP_PROP_VMB_EXPOSURE_MIN
    CV_CAP_PROP_VMB_EXPOSURE_MAX
    CV_CAP_PROP_VMB_GAMMA
    CV_CAP_PROP_VMB_HUE
    CV_CAP_PROP_VMB_SATURATION
    CV_CAP_PROP_VMB_BLACKLEVEL
    CV_CAP_PROP_VMB_TRIGGER_TYPE
    CV_CAP_PROP_VMB_TRIGGER_VAL
    CV_CAP_PROP_VMB_TRIGGER_SOURCE
    CV_CAP_PROP_VMB_SUB_BOTTOM
    CV_CAP_PROP_VMB_SUB_LEFT
    CV_CAP_PROP_VMB_SUB_RIGHT
    CV_CAP_PROP_VMB_SUB_TOP
    CV_CAP_PROP_VMB_OFFSET_X
    CV_CAP_PROP_VMB_OFFSET_Y
    CV_CAP_PROP_VMB_GAIN_MIN
    CV_CAP_PROP_VMB_GAIN_MAX            //max gain value under auto mode
    CV_CAP_PROP_VMB_ACQ_MODE            //acquisition mode
    CV_CAP_PROP_VMB_TEMPERATURE
*/
double VideoCapturer_AlliedVision::get(int propId)
{
    double d_val = 0.0;
    VmbInt64_t i_val = 0;

    switch (propId)
    {
    case CV_CAP_PROP_VMB_FPS:
        err = camera->GetAcquisitionFrameRateAbs(property.v_fps);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetAcquisitionFrameRateAbs"<<std::endl;
        else
            return property.v_fps;
        break;

    case CV_CAP_PROP_VMB_FPS_LIMIT:
        d_val = 0.0;
        err = camera->GetAcquisitionFrameRateLimit(d_val);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetAcquisitionFrameRateLimit"<<std::endl;
        else
            return d_val;
        break;

    case CV_CAP_PROP_VMB_FRAME_WIDTH:
        err = camera->GetWidth(property.v_width);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetWidth"<<std::endl;
        else
            return property.v_width;
        break;

    case CV_CAP_PROP_VMB_FRAME_HEIGHT:
        err = camera->GetHeight(property.v_height);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetHeight"<<std::endl;
        else
            return property.v_height;
        break;

    case CV_CAP_PROP_VMB_PIXELFORMAT:
        err = camera->GetPixelFormat(property.v_pixelformat);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetPixelFormat"<<std::endl;
        else   
            return (double) property.v_pixelformat;
        break;

    case CV_CAP_PROP_VMB_GAIN_AUTO:
        err = camera->GetGainAuto(property.v_gainmode);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetGainAuto"<<std::endl;
        else
            return (double) property.v_gainmode;
        break;

    case CV_CAP_PROP_VMB_GAIN_VAL:
        err = camera->GetGain(property.v_gain);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetGain"<<std::endl;
        else
            return property.v_gain;
        break;

    case CV_CAP_PROP_VMB_WB_AUTO:
        err = camera->GetBalanceWhiteAuto(property.v_wbalancemode);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetBalanceWhiteAuto"<<std::endl;
        else
            return (double) property.v_wbalancemode;
        break;

    case CV_CAP_PROP_VMB_WB_VAL:
        err = camera->GetBalanceRatioAbs(property.v_whiteBalance);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetBalanceRatioAbs"<<std::endl;
        else
            return property.v_whiteBalance;
        break;

    case CV_CAP_PROP_VMB_EXPOSURE_AUTO:
        err = camera->GetExposureAuto(property.v_exposermode);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetExposureAuto"<<std::endl;
        else
            return (double) property.v_exposermode;
        break;

    case CV_CAP_PROP_VMB_EXPOSURE_VAL:
        err = camera->GetExposureTimeAbs(property.v_exposer);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetExposureTimeAbs"<<std::endl;
        else
            return property.v_exposer;
        break;

    case CV_CAP_PROP_VMB_EXPOSURE_MIN:
        err = camera->GetExposureAutoMin(i_val);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetExposureAutoMin"<<std::endl;
        else
            return (double) i_val;
        break;

    case CV_CAP_PROP_VMB_EXPOSURE_MAX:
        err = camera->GetExposureAutoMax(i_val);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetExposureAutoMax"<<std::endl;
        else
            return (double) i_val;
        break;

    case CV_CAP_PROP_VMB_GAMMA:
        err = camera->GetGamma(property.v_Gamma);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetGamma"<<std::endl;
        else
            return property.v_Gamma;
        break;

    case CV_CAP_PROP_VMB_HUE:
        err = camera->GetHue(property.v_Hue);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetHue"<<std::endl;
        else
            return property.v_Hue;
        break;

    case CV_CAP_PROP_VMB_SATURATION:
        err = camera->GetSaturation(property.v_Saturation);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetSaturation"<<std::endl;
        else
            return property.v_Saturation;
        break;

    case CV_CAP_PROP_VMB_BLACKLEVEL:
        err = camera->GetBlackLevel(property.v_blackLevel);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetBlackLevel"<<std::endl;
        else
            return property.v_blackLevel;
        break;

    case CV_CAP_PROP_VMB_TEMPERATURE:
        err = camera->GetDeviceTemperature(property.v_temperature);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetDeviceTemperature"<<std::endl;
        else
            return property.v_temperature;
        break;

    case CV_CAP_PROP_VMB_TRIGGER_SELECT:
        err = camera->GetTriggerSelector(property.v_trigger_select);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetTriggerSelector"<<std::endl;
        else
            return (double) property.v_trigger_select;
        break;

    case CV_CAP_PROP_VMB_TRIGGER_MODE:
        err = camera->GetTriggerMode(property.v_trigger_mode);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetTriggerMode"<<std::endl;
        else
            return (double) property.v_trigger_mode;
        break;

    case CV_CAP_PROP_VMB_TRIGGER_VAL:
        err = camera->GetTriggerDelayAbs(property.v_trigger_value);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetTriggerDelayAbs"<<std::endl;
        else
            return property.v_trigger_value;
        break;

    case CV_CAP_PROP_VMB_TRIGGER_SOURCE:
        err = camera->GetTriggerSource(property.v_trigger_source);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetTriggerSource"<<std::endl;
        else
            return (int) property.v_trigger_source;
        break;

    case CV_CAP_PROP_VMB_SUB_BOTTOM:
        err = camera->GetDSPSubregionBottom(i_val);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetDSPSubregionBottom"<<std::endl;
        else
            return (double) i_val;
        break;

    case CV_CAP_PROP_VMB_SUB_LEFT:
        err = camera->GetDSPSubregionLeft(i_val);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetDSPSubregionLeft"<<std::endl;
        else
            return (double) i_val;
        break;

    case CV_CAP_PROP_VMB_SUB_RIGHT:
        err = camera->GetDSPSubregionRight(i_val);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetDSPSubregionRight"<<std::endl;
        else
            return (double) i_val;
        break;

    case CV_CAP_PROP_VMB_SUB_TOP:
        err = camera->GetDSPSubregionTop(i_val);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetDSPSubregionTop"<<std::endl;
        else
            return (double) i_val;
        break;

    case CV_CAP_PROP_VMB_OFFSET_X:
        err = camera->GetOffsetX(i_val);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetOffsetX"<<std::endl;
        else
            return (double) i_val;
        break;

    case CV_CAP_PROP_VMB_OFFSET_Y:
        err = camera->GetOffsetY(i_val);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetOffsetY"<<std::endl;
        else
            return (double) i_val;
        break;

    case CV_CAP_PROP_VMB_GAIN_MIN:
        err = camera->GetGainAutoMin(d_val);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetGainAutoMin"<<std::endl;
        else
            return d_val;
        break;

    case CV_CAP_PROP_VMB_GAIN_MAX:
        err = camera->GetGainAutoMax(d_val);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetGainAutoMax"<<std::endl;
        else
            return d_val;
        break;

    case CV_CAP_PROP_VMB_ACQ_MODE:
        err = camera->GetAcquisitionMode(property.v_acquisitionMode);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on GetAcquisitionMode"<<std::endl;
        else
            return (double) property.v_acquisitionMode;
        break;


    default:
        return -1; //bad argument
        break;
    }
    return -1;  //no
}

void VideoCapturer_AlliedVision::close()
{
    camera->Close();
    return;
}

bool VideoCapturer_AlliedVision::set_WB_red(double val)
{
    err = camera->SetBalanceWhiteAuto(MakoCamera::BalanceWhiteAuto_Off);     //OFF
    if(VmbErrorSuccess != err)
    {
        std::cerr<<"MakoCamera :: error on set_WB_red"<<std::endl;
        return false;
    }

    err = camera->SetBalanceRatioSelector(MakoCamera::BalanceRatioSelector_Red); //BalanceRatioSelector_Red = 0; BalanceRatioSelector_Blue = 1;
    if(VmbErrorSuccess != err)
    {
        std::cerr<<"MakoCamera :: error on set_WB_red"<<std::endl;
        return false;
    }
    err = camera->SetBalanceRatioAbs(val);
    if(VmbErrorSuccess != err)
    {
        std::cerr<<"MakoCamera :: error on set_WB_red"<<std::endl;
        return false;
    } else
        return true;
}

bool VideoCapturer_AlliedVision::set_WB_blue(double val)
{
    err = camera->SetBalanceWhiteAuto(MakoCamera::BalanceWhiteAuto_Off);     //OFF
    if(VmbErrorSuccess != err)
    {
        std::cerr<<"MakoCamera :: error on set_WB_red"<<std::endl;
        return false;
    }

    err = camera->SetBalanceRatioSelector(MakoCamera::BalanceRatioSelector_Blue); //BalanceRatioSelector_Red = 0; BalanceRatioSelector_Blue = 1;
    if(VmbErrorSuccess != err)
    {
        std::cerr<<"MakoCamera :: error on set_WB_red"<<std::endl;
        return false;
    }
    err = camera->SetBalanceRatioAbs(val);
    if(VmbErrorSuccess != err)
    {
        std::cerr<<"MakoCamera :: error on set_WB_red"<<std::endl;
        return false;
    } else
        return true;
}

bool VideoCapturer_AlliedVision::set_WB_mode_red(int mode)
{
    err = camera->SetBalanceRatioSelector(MakoCamera::BalanceRatioSelector_Red); //BalanceRatioSelector_Red = 0; BalanceRatioSelector_Blue = 1;
    if(VmbErrorSuccess != err)
    {
        std::cerr<<"MakoCamera :: error on set_WB_red"<<std::endl;
        return false;
    }
    err = camera->SetBalanceWhiteAuto((MakoCamera::BalanceWhiteAutoEnum) mode);     //OFF
    if(VmbErrorSuccess != err)
    {
        std::cerr<<"MakoCamera :: error on set_WB_red"<<std::endl;
        return false;
    }

}
bool VideoCapturer_AlliedVision::set_WB_mode_blue(int mode)
{
    err = camera->SetBalanceRatioSelector(MakoCamera::BalanceRatioSelector_Blue); //BalanceRatioSelector_Red = 0; BalanceRatioSelector_Blue = 1;
    if(VmbErrorSuccess != err)
    {
        std::cerr<<"MakoCamera :: error on set_WB_blue"<<std::endl;
        return false;
    }
    err = camera->SetBalanceWhiteAuto((MakoCamera::BalanceWhiteAutoEnum) mode);     //OFF
    if(VmbErrorSuccess != err)
    {
        std::cerr<<"MakoCamera :: error on set_WB_blue"<<std::endl;
        return false;
    }
}



double VideoCapturer_AlliedVision::get_WB_red()
{
    double val = 0;
    err = camera->SetBalanceRatioSelector(MakoCamera::BalanceRatioSelector_Red); //BalanceRatioSelector_Red = 0; BalanceRatioSelector_Blue = 1;
    err = camera->GetBalanceRatioAbs(val);
    if(VmbErrorSuccess != err)
    {
        std::cerr<<"MakoCamera :: error on set_WB_red"<<std::endl;
        return 0.0;
    } else
        return val;
}

double VideoCapturer_AlliedVision::get_WB_blue()
{
    double val = 0;
    err = camera->SetBalanceRatioSelector(MakoCamera::BalanceRatioSelector_Red); //BalanceRatioSelector_Red = 0; BalanceRatioSelector_Blue = 1;
    err = camera->GetBalanceRatioAbs(val);
    if(VmbErrorSuccess != err)
    {
        std::cerr<<"MakoCamera :: error on set_WB_red"<<std::endl;
        return 0.0;
    } else
        return val;
}

bool VideoCapturer_AlliedVision::set_WB_autoOnce()
{
    err = camera->SetBalanceRatioSelector(MakoCamera::BalanceRatioSelector_Red); //BalanceRatioSelector_Red = 0; BalanceRatioSelector_Blue = 1;
    err = camera->SetBalanceWhiteAuto(MakoCamera::BalanceWhiteAuto_Once); //BalanceWhiteAuto_Once
    err = camera->SetBalanceRatioSelector(MakoCamera::BalanceRatioSelector_Blue); //BalanceRatioSelector_Red = 0; BalanceRatioSelector_Blue = 1;
    err = camera->SetBalanceWhiteAuto(MakoCamera::BalanceWhiteAuto_Once); //BalanceWhiteAuto_Once
    if(VmbErrorSuccess != err)
    {
        std::cerr<<"MakoCamera :: error on set_WB_red"<<std::endl;
        return false;
    }
    else
        return true;
}

bool VideoCapturer_AlliedVision::set_WB_autoContinuous()
{
    err = camera->SetBalanceRatioSelector(MakoCamera::BalanceRatioSelector_Red); //BalanceRatioSelector_Red = 0; BalanceRatioSelector_Blue = 1;
    err = camera->SetBalanceWhiteAuto(MakoCamera::BalanceWhiteAuto_Continuous);  //BalanceWhiteAuto_Continuous
    err = camera->SetBalanceRatioSelector(MakoCamera::BalanceRatioSelector_Blue);//BalanceRatioSelector_Red = 0; BalanceRatioSelector_Blue = 1;
    err = camera->SetBalanceWhiteAuto(MakoCamera::BalanceWhiteAuto_Continuous);  //BalanceWhiteAuto_Continuous
    if(VmbErrorSuccess != err)
    {
        std::cerr<<"MakoCamera :: error on set_WB_red"<<std::endl;
        return false;
    }
    else
        return true;
}

std::string VideoCapturer_AlliedVision::get_IDCamera()
{
    std::string name_ID;
    err = camera->GetDeviceID(name_ID);
    if(VmbErrorSuccess != err)
    {
        std::cerr<<"MakoCamera :: error on get_nameCamera"<<std::endl;
        return "none";
    }
    else
        return name_ID;
}



/*!
  \brief Set parameter of the camera.

  \param propId is the indicator of the parameter to read.
  \param value is the value for the parameter.
  \return true if the parameter was set.

  \note Possible values of the "propId":
    CV_CAP_PROP_VMB_FPS
    CV_CAP_PROP_VMB_FPS_LIMIT
    CV_CAP_PROP_VMB_FRAME_WIDTH
    CV_CAP_PROP_VMB_FRAME_HEIGHT
    CV_CAP_PROP_VMB_PIXELFORMAT
    CV_CAP_PROP_VMB_GAIN_AUTO
    CV_CAP_PROP_VMB_GAIN_VAL
    CV_CAP_PROP_VMB_WB_AUTO
    CV_CAP_PROP_VMB_WB_VAL
    CV_CAP_PROP_VMB_EXPOSURE_AUTO
    CV_CAP_PROP_VMB_EXPOSURE_VAL
    CV_CAP_PROP_VMB_EXPOSURE_MIN
    CV_CAP_PROP_VMB_EXPOSURE_MAX
    CV_CAP_PROP_VMB_GAMMA
    CV_CAP_PROP_VMB_HUE
    CV_CAP_PROP_VMB_SATURATION
    CV_CAP_PROP_VMB_BLACKLEVEL
    CV_CAP_PROP_VMB_TRIGGER_TYPE
    CV_CAP_PROP_VMB_TRIGGER_VAL
    CV_CAP_PROP_VMB_TRIGGER_SOURCE
    CV_CAP_PROP_VMB_TRIGGER_MODE
    CV_CAP_PROP_VMB_SUB_BOTTOM
    CV_CAP_PROP_VMB_SUB_LEFT
    CV_CAP_PROP_VMB_SUB_RIGHT
    CV_CAP_PROP_VMB_SUB_TOP
    CV_CAP_PROP_VMB_OFFSET_X
    CV_CAP_PROP_VMB_OFFSET_Y
    CV_CAP_PROP_VMB_GAIN_MIN
    CV_CAP_PROP_VMB_GAIN_MAX            //max gain value under auto mode
    CV_CAP_PROP_VMB_ACQ_MODE            //acquisition mode
    CV_CAP_PROP_VMB_TEMPERATURE
*/
bool VideoCapturer_AlliedVision::set(int propId, double value)
{
    switch (propId)
    {
    case CV_CAP_PROP_VMB_FPS:
        err = camera->SetAcquisitionFrameRateAbs(value);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetAcquisitionFrameRateAbs"<<std::endl;
        else
        {
            property.v_fps = value;
            return true;
        }
        break;

    case CV_CAP_PROP_VMB_FRAME_WIDTH:
        err = camera->SetWidth(value);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetWidth"<<std::endl;
        else
        {
            property.v_width = value;
            return true;
        }
        break;

    case CV_CAP_PROP_VMB_FRAME_HEIGHT:
        err = camera->SetHeight(value);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetHeight"<<std::endl;
        else
        {
            property.v_height = value;
            return true;
        }
        break;

    case CV_CAP_PROP_VMB_PIXELFORMAT:
    {
        MakoCamera::PixelFormatEnum ax_i= (MakoCamera::PixelFormatEnum) cvRound(value) ;
        err = camera->SetPixelFormat(ax_i);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetPixelFormat"<<std::endl;
        else
        {
            property.v_pixelformat = (MakoCamera::PixelFormatEnum) cvRound(value);
            return true;
        }
        break;
    }

    case CV_CAP_PROP_VMB_GAIN_AUTO:
    {
        MakoCamera::GainAutoEnum ax_i= (MakoCamera::GainAutoEnum) cvRound(value) ;
        err = camera->SetGainAuto(ax_i);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetGainAuto"<<std::endl;
        else
        {
            err = camera->SetPixelFormat(property.v_pixelformat); //reset pixelformat
            property.v_gainmode = (MakoCamera::GainAutoEnum) cvRound(value);
            return true;
        }
        break;
    }

    case CV_CAP_PROP_VMB_GAIN_VAL:
        err = camera->SetGain(value);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetGain"<<std::endl;
        else
        {
            property.v_gain = value;
            return true;
        }
        break;

    case CV_CAP_PROP_VMB_WB_AUTO:
        err = camera->SetBalanceWhiteAuto((MakoCamera::BalanceWhiteAutoEnum) cvRound(value));
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetBalanceWhiteAuto"<<std::endl;
        else
        {
            property.v_wbalancemode = (MakoCamera::BalanceWhiteAutoEnum) cvRound(value);
            return true;
        }
        break;


    case CV_CAP_PROP_VMB_WB_VAL:
        err = camera->SetBalanceRatioAbs(value);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetBalanceRatioAbs"<<std::endl;
        else
        {
            property.v_whiteBalance = value;
            return true;
        }
        break;


    case CV_CAP_PROP_VMB_EXPOSURE_AUTO:
    {
        MakoCamera::ExposureAutoEnum aux = (MakoCamera::ExposureAutoEnum) cvRound(value);
        err = camera->SetExposureAuto(aux);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetExposureAuto"<<std::endl;
        else
        {
            property.v_exposermode = (MakoCamera::ExposureAutoEnum) cvRound(value);
            err = camera->SetHeight(property.v_height);  //reset Height
            err = camera->SetWidth(property.v_width);    //reset Width
            err = camera->SetPixelFormat(property.v_pixelformat); //reset pixelformat
            return true;
        }
        break;
    }

    case CV_CAP_PROP_VMB_EXPOSURE_VAL:
        err = camera->SetExposureTimeAbs(value);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetExposureTimeAbs"<<std::endl;
        else
        {
            property.v_exposer = value;
            return true;
        }
        break;

    case CV_CAP_PROP_VMB_EXPOSURE_MIN:
        err = camera->SetExposureAutoMin((VmbInt64_t) cvRound(value));
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetExposureAutoMin"<<std::endl;
        else
            return true;
        break;

    case CV_CAP_PROP_VMB_EXPOSURE_MAX:
        err = camera->SetExposureAutoMax((VmbInt64_t)  cvRound(value));
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetExposureAutoMax"<<std::endl;
        else
            return true;
        break;

    case CV_CAP_PROP_VMB_GAMMA:
        err = camera->SetGamma(value);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetGamma"<<std::endl;
        else
        {
            property.v_Gamma = value;
            return true;
        }
        break;

    case CV_CAP_PROP_VMB_HUE:
        err = camera->SetHue(value);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetHue"<<std::endl;
        else
        {
            property.v_Hue = value;
            return true;
        }
        break;


    case CV_CAP_PROP_VMB_SATURATION:
        err = camera->SetSaturation(value);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetSaturation"<<std::endl;
        else
        {
            property.v_Saturation = value;
            return true;
        }
        break;

    case CV_CAP_PROP_VMB_BLACKLEVEL:
        err = camera->SetBlackLevel(value);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetBlackLevel"<<std::endl;
        else
        {
            property.v_blackLevel = value;
            return true;
        }
        break;

    case CV_CAP_PROP_VMB_TRIGGER_SELECT:
        err = camera->SetTriggerSelector( (MakoCamera::TriggerSelectorEnum) cvRound(value));
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetTriggerSelector"<<std::endl;
        else
        {
            property.v_trigger_select = (MakoCamera::TriggerSelectorEnum) cvRound(value);
            return true;
        }
        break;

    case CV_CAP_PROP_VMB_TRIGGER_VAL:
        err = camera->SetTriggerDelayAbs(value);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetTriggerDelayAbs"<<std::endl;
        else
        {
            property.v_trigger_value = value;
            return true;
        }
        break;

    case CV_CAP_PROP_VMB_TRIGGER_SOURCE:
        err = camera->SetTriggerSource((MakoCamera::TriggerSourceEnum) cvRound(value));
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetTriggerSource:"<<cvRound(value)<<std::endl;
        else
        {
            property.v_trigger_source = (MakoCamera::TriggerSourceEnum) cvRound(value);
            return true;
        }
        break;

    case CV_CAP_PROP_VMB_TRIGGER_MODE:
        err = camera->SetTriggerMode((MakoCamera::TriggerModeEnum) cvRound(value));
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetTriggerMode:"<<cvRound(value)<<std::endl;
        else
        {
            property.v_trigger_mode = (MakoCamera::TriggerModeEnum) cvRound(value);
            return true;
        }
        break;

    case CV_CAP_PROP_VMB_SUB_BOTTOM:
        err = camera->SetDSPSubregionBottom((VmbInt64_t) cvRound(value));
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetDSPSubregionBottom"<<std::endl;
        else
        {
            property.subRegion_br.y = cvRound(value);
            return true;
        }
        break;

    case CV_CAP_PROP_VMB_SUB_LEFT:
        err = camera->SetDSPSubregionLeft((VmbInt64_t) cvRound(value));
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetDSPSubregionLeft"<<std::endl;
        else
        {
            property.subRegion_tl.x = cvRound(value);
            return true;
        }
        break;

    case CV_CAP_PROP_VMB_SUB_RIGHT:
        err = camera->SetDSPSubregionRight((VmbInt64_t) cvRound(value));
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetDSPSubregionRight"<<std::endl;
        else
        {
            property.subRegion_br.x = cvRound(value);
            return true;
        }
        break;

    case CV_CAP_PROP_VMB_SUB_TOP:
        err = camera->SetDSPSubregionTop((VmbInt64_t) cvRound(value));
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetDSPSubregionTop"<<std::endl;
        else
        {
            property.subRegion_tl.y = cvRound(value);
            return true;
        }
        break;

    case CV_CAP_PROP_VMB_OFFSET_X:
        err = camera->SetOffsetX((VmbInt64_t) cvRound(value));
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetOffsetX:"<<value<<std::endl;
        else
            return true;
        break;

    case CV_CAP_PROP_VMB_OFFSET_Y:
        err = camera->SetOffsetY((VmbInt64_t) cvRound(value));
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetOffsetY:"<<value<<std::endl;
        else
            return true;
        break;

    case CV_CAP_PROP_VMB_GAIN_MIN:
        err = camera->SetGainAutoMin(value);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetGainAutoMin"<<std::endl;
        else
            return true;
        break;

    case CV_CAP_PROP_VMB_GAIN_MAX:
        err = camera->SetGainAutoMax(value);
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetGainAutoMax"<<std::endl;
        else
            return true;
        break;

    case CV_CAP_PROP_VMB_ACQ_MODE:
        err = camera->SetAcquisitionMode((MakoCamera::AcquisitionModeEnum) cvRound(value));
        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on SetAcquisitionMode"<<std::endl;
        else
        {
            property.v_acquisitionMode = (MakoCamera::AcquisitionModeEnum) cvRound(value);
            return true;
        }
        break;

    default:
        return false;
        break;
    }

    return false;
}





void VideoCapturer_AlliedVision::set_frame_Properties()
{
    //! Config PixelFormat.
    err = camera->GetPixelFormat(property.v_pixelformat);
    if(VmbErrorSuccess != err)
        std::cerr<<"MakoCamera :: error on GetPixelFormat"<<std::endl;


    // Set pixel format. For the sake of simplicity we only support Mono8 and BayerBG8 in this example.
    if ( property.v_pixelformat != MakoCamera::PixelFormat_BayerRG8 )
    //if ( v_pixelformat != MakoCamera::PixelFormat_RGB8Packed )
    //    if ( property.v_pixelformat != MakoCamera::PixelFormat_RGB8Packed )
    {
        // Try to set BayerBG8
        err = camera->SetPixelFormat(MakoCamera::PixelFormat_BayerRG8 );
        //err = camera->SetPixelFormat(MakoCamera::PixelFormat_RGB8Packed );
//        err = camera->SetPixelFormat(MakoCamera::PixelFormat_RGB8Packed );
        if ( VmbErrorSuccess != err )
        {
            std::cerr<<"MakoCamera :: error on PixelFormat_BayerBG8 -> fallback to Mono8"<<std::endl;
            // Fall back to Mono
            err = camera->SetPixelFormat( MakoCamera::PixelFormat_Mono8 );
        }

        if(VmbErrorSuccess != err)
            std::cerr<<"MakoCamera :: error on PixelFormat_Mono8"<<std::endl;
        err = camera->GetPixelFormat(property.v_pixelformat);  //read again
    }
    return;
}



bool VideoCapturer_AlliedVision::grab()
{
    err = camera->AcquireSingleImage( pFrame, property.v_timeout_frame);
    if ( VmbErrorSuccess == err )
        hasNewFrameToDecode = true;
    else
        hasNewFrameToDecode = false;
    return hasNewFrameToDecode;
}

bool VideoCapturer_AlliedVision::retrieve(cv::Mat &image, int channel)
{
    if(hasNewFrameToDecode)
    {
        ConvertFrameToImage(pFrame, image, false);   //decodes the frame to cv::Mat
        hasNewFrameToDecode = false;
        return true;
    }
    else
    {
        hasNewFrameToDecode = false;
        return false;
    }
}

cv::VideoCapture &VideoCapturer_AlliedVision::operator >>(cv::Mat &image)
{
    this->read(image);
}



bool VideoCapturer_AlliedVision::read(cv::Mat &image)
{
    if(!isStreaming)  ///if singleframe mode selected
    {
        err = camera->AcquireSingleImage( pFrame, property.v_timeout_frame);
        if ( VmbErrorSuccess == err )
        {
            err = ConvertFrameToImage(pFrame, image, false);   //decodes the frame to cv::Mat
            if ( VmbErrorSuccess == err )
                return true;
            else
                return false;
        }
        else
            return false;
    }
    else             /// if continuousframe mode selected
    {
        if (hasNewFrameToDecode)
        {
            image = img;
            return true;
        }
        else
            return false;
    }
}



void VideoCapturer_AlliedVision::printListOfCameras()
{
    camID.clear();
    camName.clear();
    camModel.clear();
    camSerialN.clear();
    camInterfID.clear();
    std::string msg;
    cameras.clear();

    err = VmbErrorIncomplete;

    if (isVimbaReady)
        err = system->GetCameras (cameras);

    if(err == VmbErrorSuccess)
    {
        std::cout<<" ************************************************\n\r           Cameras detected:\n\r ************************************************\n  "<<std::endl;
        for ( AVT::VmbAPI::CameraPtrVector::iterator iter = cameras.begin (); cameras.end () != iter; ++ iter )
        {
            std::cout<<" --> CAMERA:: ";
            if ( VmbErrorSuccess == (* iter)->GetID(msg) )
            {
                camID.push_back(msg);
                std::cout << " ID:"<<msg;
            }
            msg.clear();

            if ( VmbErrorSuccess == (* iter)->GetName(msg) )
            {
                camName.push_back(msg);
                std::cout <<" Name:"<<msg;
            }
            msg.clear();

            if ( VmbErrorSuccess == (* iter)->GetModel(msg) )
            {
                camModel.push_back(msg);
                std::cout <<"  Model:"<<msg;
            }
            msg.clear();

            if ( VmbErrorSuccess == (* iter)->GetSerialNumber(msg) )
            {
                camSerialN.push_back(msg);
                std::cout <<"  SerialNumb:"<<msg;
            }
            msg.clear();

            if ( VmbErrorSuccess == (* iter)->GetInterfaceID (msg) )
            {
                camInterfID.push_back(msg);
                std::cout <<"  Interface:"<<msg;
            }
            msg.clear();
            std::cout<<std::endl;
        }
    }
    return;
}


/*! \brief SLOT that captures the signal from the Cameraobserver.
 *         Indicates when the list of available cameras is changed.
 */
void VideoCapturer_AlliedVision::slot_updateDeviceList()
{
    std::string connected_CamID = camID[idx_connectedToDevice];

    std::cout<<"VideoCapturer_AlliedVision :: slot_updateDeviceList "<<std::endl;
    printListOfCameras();

    bool isKeepedPlugged = false;
    //See if the same camera was unplugged
    for(unsigned i = 0; i < camID.size(); i++)
    {
        //No
        if(camID[i] == connected_CamID)
        {
            idx_connectedToDevice = i;
            isKeepedPlugged = true;
            break;
        }
    }

    if(!isKeepedPlugged)
    {
        isCameraOpen = false;
        camera->Close();
        std::cerr<<"VideoCapturer_AlliedVision :: slot_updateDeviceList :: Camera Closed"<<std::endl;
    }
    return;
}


void VideoCapturer_AlliedVision::slot_FrameReady(void)
{
    if ( isStreaming )
    {
        // Pick up frame
        pFrame = pFrameObserver->GetFrame();

        err = ConvertFrameToImage(pFrame, img, false);   //decodes the frame to cv::Mat
        if ( VmbErrorSuccess == err )
            hasNewFrameContinuous = true;
        else
            hasNewFrameContinuous = false;

        // And queue it to continue streaming
        camera->QueueFrame( pFrame );

        emit sig_EventFrameReady();   //emit signal for external classes. Notify that new frame is available in the opencv format.
    }

    return;
}




/*! \brief Convert 12-bit Bayer image to 8-bit RGB for OpenCV.
 *
 *
 *  \param pInBuffer is unsigned char pointer that contains the data of the frame captured by camera.
 *  \var OutImage is the cv::Mat as CV_8UC1 or CV_8UC3.
 *
 *  \warning the size of the OutImage must be defined before this function. The format could be different!!!
 */
void VideoCapturer_AlliedVision::CopyBayerToImage_12b_to_8b( VmbUchar_t *pInBuffer, cv::Mat & OutImage)
{
    int height = OutImage.rows, width = OutImage.cols;

    if(OutImage.channels() == 3)
    {
        // Copy the data into an OpenCV Mat structure
        cv::Mat bayer16BitMat(height, width, CV_16UC1, pInBuffer);

        // Convert the Bayer data from 16-bit to to 8-bit
        cv::Mat bayer8BitMat = bayer16BitMat.clone();
        // The 3rd parameter here scales the data by 1/16 so that it fits in 8 bits.
        bayer8BitMat.convertTo(bayer8BitMat, CV_8UC1, 0.0625);   //NOTE:: this value was compared with results of the VimbaViwer.

        // Convert the Bayer data to 8-bit RGB
        OutImage = cv::Mat::zeros(height, width, CV_8UC3);
        cv::cvtColor(bayer8BitMat, OutImage, cv::COLOR_BayerRG2RGB);
    }
    else
    {
        // Copy the data into an OpenCV Mat structure
        cv::Mat bayer16BitMat(height, width, CV_16UC1, pInBuffer);

        // Convert the Bayer data from 16-bit to to 8-bit
        cv::Mat bayer8BitMat = bayer16BitMat.clone();
        // The 3rd parameter here scales the data by 1/16 so that it fits in 8 bits.
        // Without it, convertTo() just seems to chop off the high order bits.
        bayer8BitMat.convertTo(OutImage, CV_8UC1, 0.0625);
    }
    return;
}


/*! \brief Convert 12-bit Bayer image to 12-bit RGB for OpenCV.
 *
 *
 *  \param pInBuffer is unsigned char pointer that contains the data of the frame captured by camera.
 *  \var OutImage is the cv::Mat as CV_16UC1 or CV_16UC3.
 *
 *  \warning the size of the OutImage must be defined before this function. The format could be different!!!
 */
void VideoCapturer_AlliedVision::CopyBayerToImage_12b_to_12b( VmbUchar_t *pInBuffer, cv::Mat & OutImage )
{
    int height = OutImage.rows, width = OutImage.cols;

    if(OutImage.channels() == 3)
    {
        // Copy the data into an OpenCV Mat structure
        cv::Mat bayer16BitMat(height, width, CV_16UC1, pInBuffer);

        // Convert the Bayer data to 16-bit RGB
        OutImage = cv::Mat::zeros(height, width, CV_16UC3);
        cv::cvtColor(bayer16BitMat, OutImage, cv::COLOR_BayerRG2RGB);
    }
    else
    {
        // Copy the data into an OpenCV Mat structure
        OutImage = cv::Mat(height, width, CV_16UC1, pInBuffer);
    }
    return;
}


/*! \brief Convert 8-bit Bayer image to 8-bit RGB for OpenCV.
 *
 *
 *  \param pInBuffer is unsigned char pointer that contains the data of the frame captured by camera.
 *  \var OutImage is the cv::Mat as CV_8UC1 or CV_8UC3.
 *
 *  \warning the size of the OutImage must be defined before this function.
 */
void VideoCapturer_AlliedVision::CopyBayerToImage_8b_to_8b( VmbUchar_t *pInBuffer, cv::Mat & OutImage )
{
    int height = OutImage.rows, width = OutImage.cols;

    if(OutImage.channels() == 3)
    {
        // Copy the data into an OpenCV Mat structure
        cv::Mat bayer8BitMat(height, width, CV_8UC1, pInBuffer);

        // Convert the Bayer data to 8-bit RGB
        OutImage = cv::Mat::zeros(height, width, CV_8UC3);
        cv::cvtColor(bayer8BitMat, OutImage, cv::COLOR_BayerRG2RGB);
    }
    else
    {
        // Copy the data into an OpenCV Mat structure
        OutImage = cv::Mat(height, width, CV_8UC1, pInBuffer);
    }
    return;
}



/*! \brief Convert frame of camera to 8-bit cv::Mat for OpenCV.
 *
 *
 *  \param pFrame isthe received frame.
 *  \var OutImage is the cv::Mat as CV_8UC1 or CV_8UC3.
 *  \param isOut12b defines if the output is 12b
 *
 *  \warning if "isOut12b == true", then
 *          cv::Mat aux(OutImage.cols, OutImage.rows, CV_8UC3);
 *          OutImage.convertTo(aux, CV_8UC3, 0.0625);
 *          cv::imshow("Live", aux);
 */
VmbErrorType VideoCapturer_AlliedVision::ConvertFrameToImage( AVT::VmbAPI::FramePtr pFrame, cv::Mat & OutImage, bool isOut12b = false)
{
    unsigned int width = 0, height = 0;
//    width = property.v_width;
//    height = property.v_height;
    pFrame->GetWidth(width);
    pFrame->GetHeight(height);


    VmbPixelFormatType pixelFormat;
    pFrame->GetPixelFormat(pixelFormat);

    if((width == 0) || (height == 0))
    {
        std::cerr<<" convert:"<<width<<" x "<<width<<" "<<height<<"  -> "<<(int) pixelFormat<<std::endl;
        return VmbErrorInternalFault;
    }
    else
    {
        /// Setup "Output image"
        switch (pixelFormat)
        {
        case VmbPixelFormatMono8:
            OutImage = cv::Mat(height, width, CV_8UC1);
            break;

        case VmbPixelFormatMono12:
            OutImage = cv::Mat(height, width, CV_16UC1); //do not worry with format
            break;

        case VmbPixelFormatBayerRG8:                   //this is the active (it should be)
            OutImage = cv::Mat(height, width, CV_8UC3);
            break;

        case VmbPixelFormatBayerRG12:
            OutImage = cv::Mat(height, width, CV_16UC3); //do not worry with format
            break;

        case VmbPixelFormatRgb8:
            OutImage = cv::Mat(height, width, CV_8UC3);
            break;

        case VmbPixelFormatBgr8:
            OutImage = cv::Mat(height, width, CV_8UC3);
            break;
        default:
            std::cerr<<" ConvertToImage :: invalid pixelformat configuration"<<std::endl;
            return VmbErrorNotFound;
        }


        /// See if frame is not corrupt
        VmbFrameStatusType eReceiveStatus;
        VmbErrorType err = pFrame->GetReceiveStatus( eReceiveStatus );

        if (VmbErrorSuccess == err && VmbFrameStatusComplete == eReceiveStatus )
        {
            //Get the data
            VmbUchar_t *pBuffer;
            err = pFrame->GetImage( pBuffer );
            if ( VmbErrorSuccess == err )
            {

                switch (pixelFormat)
                {
                case VmbPixelFormatMono8:
                    OutImage = cv::Mat(height, width, CV_8UC1, pBuffer);
                    break;

                case VmbPixelFormatMono12:
                    if(!isOut12b)
                        CopyBayerToImage_12b_to_8b( pBuffer, OutImage);
                    else
                        CopyBayerToImage_12b_to_12b( pBuffer, OutImage);
                    break;

                case VmbPixelFormatBayerRG8:
                    CopyBayerToImage_8b_to_8b( pBuffer, OutImage);
                    break;

                case VmbPixelFormatBayerRG12:
                    if(!isOut12b)
                        CopyBayerToImage_12b_to_8b( pBuffer, OutImage);
                    else
                        CopyBayerToImage_12b_to_12b( pBuffer, OutImage);
                    break;

                case VmbPixelFormatRgb8:
                    OutImage = cv::Mat(height, width, CV_8UC3, pBuffer);
                    cv::cvtColor(OutImage, OutImage, cv::COLOR_RGB2BGR);
                    break;

                case VmbPixelFormatBgr8:
                    OutImage = cv::Mat(height, width, CV_8UC3, pBuffer);
                    break;

                default:
                    std::cerr<<" ConvertToImage :: invalid pixelformat configuration"<<std::endl;
                    return VmbErrorNotFound;
                }
            }
            else
                return VmbErrorInternalFault;
        }
        else
            return VmbErrorInternalFault;
    }
    return VmbErrorSuccess;
}









//*********************************************************
//*********************************************************
//*********************************************************
//*************************************************** ASYNC

VmbErrorType VideoCapturer_AlliedVision::StartContinuousImageAcquisition(int device)
{
    if (!isVimbaReady)
        initSetup();

    if(!isCameraOpen)
        this->open(device);


    if(isCameraOpen)
    {
        if(!set(CV_CAP_PROP_VMB_ACQ_MODE, MakoCamera::AcquisitionMode_Continuous))
            std::cerr<<" VideoCapturer_AlliedVision:: async_start :: set to continuous mode failed"<<std::endl;

        // Create a frame observer for this camera (This will be wrapped in a shared_ptr so we don't delete it)
        if(pFrameObserver == NULL)
            pFrameObserver = new FrameObserver(camera);


        // Set the GeV packet size to the highest possible value
        // (In this example we do not test whether this cam actually is a GigE cam)
        AVT::VmbAPI::FeaturePtr pCommandFeature;
        if ( VmbErrorSuccess == camera->GetFeatureByName( "GVSPAdjustPacketSize", pCommandFeature ))
        {
            if ( VmbErrorSuccess == pCommandFeature->RunCommand() )
            {
                bool bIsCommandDone = false;
                do
                {
                    if ( VmbErrorSuccess != pCommandFeature->IsCommandDone( bIsCommandDone ))
                    {
                        break;
                    }
                }
                while( false == bIsCommandDone );
            }
        }

        AVT::VmbAPI::FeaturePtr pFormatFeature;
        // Save the current width
        err = camera->GetFeatureByName( "Width", pFormatFeature );
        if ( VmbErrorSuccess == err )
        {
            err = pFormatFeature->GetValue( this->property.v_width );
            if ( VmbErrorSuccess == err )
            {
                // Save current height
                err = camera->GetFeatureByName( "Height", pFormatFeature );
                if ( VmbErrorSuccess == err )
                {
                    pFormatFeature->GetValue( this->property.v_height );
                    if ( VmbErrorSuccess == err )
                    {
                        // Set pixel format. For the sake of simplicity we only support Mono and RGB in this example.
                        err = camera->GetFeatureByName( "PixelFormat", pFormatFeature );
                        if ( VmbErrorSuccess == err )
                        {
                            // Try to set RGB
                            err = pFormatFeature->SetValue( VmbPixelFormatBayerRG8 );
                            if ( VmbErrorSuccess != err )
                            {
                                // Fall back to Mono
                                err = pFormatFeature->SetValue( VmbPixelFormatMono8 );
                            }

                            // Read back the currently selected pixel format
                            pFormatFeature->GetValue( m_nPixelFormat );

                            if ( VmbErrorSuccess == err )
                            {
                                // Start streaming
                                err = camera->StartContinuousImageAcquisition( NUM_FRAMES, AVT::VmbAPI::IFrameObserverPtr( pFrameObserver ));
                                if ( VmbErrorSuccess == err )
                                {
                                    QObject::connect(this->pFrameObserver, SIGNAL(sig_newFrame()), this, SLOT(slot_FrameReady()) );
                                    isStreaming = true;
                                }
                                else
                                {
                                    isStreaming = false;
                                    std::cout<<" ERROR"<<std::endl;
                                }

                            }
                        }
                    }
                }
            }
        }

    }
    else
    {
        isStreaming = false;
        err = VmbErrorDeviceNotOpen;
    }

    return err;

}



VmbErrorType VideoCapturer_AlliedVision::StopContinuousImageAcquisition()
{
    // Stop streaming
    camera->StopContinuousImageAcquisition();

    // Clears all remaining frames that have not been picked up
    pFrameObserver->ClearFrameQueue();

    isStreaming = false;

    // Close camera
    return  camera->Close();
}



bool VideoCapturer_AlliedVision::readContinuous(cv::Mat &image)
{
    if(isStreaming)  ///if singleframe mode selected
    {
        if(hasNewFrameContinuous)
        {
            image = img;
            hasNewFrameContinuous = false;
            return true;
        }
        else
            return false;
    }
    else
        return false;
}












// ************************************************************************************************************** //
// ************************************************************************************************************** //
//                                  CameraObserver : sends SIGNAL: sig_updateDeviceList
// ************************************************************************************************************** //
// ************************************************************************************************************** //

CameraObserver::CameraObserver()
{
}

CameraObserver::~CameraObserver()
{
}

void CameraObserver::CameraListChanged(AVT::VmbAPI::CameraPtr pCam, AVT::VmbAPI::UpdateTriggerType reason)
{
    emit sig_updateDeviceList();
    return;
}











// ************************************************************************************************************** //
// ************************************************************************************************************** //
//                                  FrameObserver : sends SIGNAL: sig_newFrame
// ************************************************************************************************************** //
// ************************************************************************************************************** //
FrameObserver::FrameObserver(AVT::VmbAPI::CameraPtr pCamera ) : AVT::VmbAPI::IFrameObserver( pCamera )
{
    isFrameReady   = false;
}


void FrameObserver::FrameReceived( const AVT::VmbAPI::FramePtr pFrame )
{
    bool bQueueDirectly = true;
    VmbFrameStatusType eReceiveStatus;

    if ( VmbErrorSuccess == pFrame->GetReceiveStatus( eReceiveStatus ))
    {
        // Lock the frame queue
        m_FramesMutex.lock();
        // Add frame to queue
        m_Frames.push( pFrame );
        // Unlock frame queue
        m_FramesMutex.unlock();


        // Emit the frame received signal
        if (eReceiveStatus == VmbFrameStatusComplete)
        {
            isFrameReady = true;
            emit sig_newFrame();
        }


        bQueueDirectly = false;
    }

    // If any error occurred we queue the frame without notification
    if ( true == bQueueDirectly )
        m_pCamera->QueueFrame( pFrame );

    return;
}

// Returns the oldest frame that has not been picked up yet
AVT::VmbAPI::FramePtr FrameObserver::GetFrame()
{
    if(isFrameReady)
    {
        // Lock the frame queue
        m_FramesMutex.lock();
        // Pop frame from queue
        AVT::VmbAPI::FramePtr res = m_Frames.front();
        // Unlock frame queue
        m_FramesMutex.unlock();

        isFrameReady = !m_Frames.empty();

        return res;
    }
    else
    {
        AVT::VmbAPI::FramePtr res;
        return res;
    }

}

void FrameObserver::ClearFrameQueue()
{
    // Lock the frame queue
    m_FramesMutex.lock();
    // Clear the frame queue and release the memory
    std::queue<AVT::VmbAPI::FramePtr> empty;
    std::swap( m_Frames, empty );
    // Unlock the frame queue
    m_FramesMutex.unlock();
    return;
}




} //namespace "Device"
